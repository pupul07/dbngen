import numpy as np
import util

def gen_scopes(doms, maxlinks):
    scopes = []
    linkdist = np.exp(np.arange(maxlinks)) / np.exp(np.arange(maxlinks)).sum()
    for v in range(doms.shape[0]):
        nlinks = np.random.choice(maxlinks, p=linkdist) + 1
        curr_scope = np.array([],dtype=int) if v==0 else np.random.choice(v, min(v,nlinks), replace=False)
        scopes += [ np.sort(np.append(curr_scope,v)) ]
    return scopes

def gen_dbn_scopes(doms, intra_maxlinks, inter_maxlinks):
    V = doms.shape[0]
    scopes = gen_scopes(doms, intra_maxlinks)
    intra_linkdist = np.exp(np.arange(intra_maxlinks)) / np.exp(np.arange(intra_maxlinks)).sum()
    inter_linkdist = np.exp(np.arange(inter_maxlinks)) / np.exp(np.arange(inter_maxlinks)).sum()
    for v in range(V):
        intra_nlinks = np.random.choice(intra_maxlinks, p=intra_linkdist) + 1
        inter_nlinks = np.random.choice(inter_maxlinks, p=inter_linkdist) + 1
        curr_scope = np.array([],dtype=int) if v==0 else np.random.choice(v, min(v,intra_nlinks), replace=False) + V
        curr_scope = np.append(curr_scope, np.random.choice(V, min(v,inter_nlinks), replace=False))
        scopes += [ np.sort(np.append(curr_scope,v+V)) ]
    return scopes

def gen_cpts(doms, scopes):
    cpts = []
    for v in range(doms.shape[0]):
        var_doms = doms[scopes[v]]
        probs = np.exp(np.random.uniform(1,5,var_doms.prod()))
        for i in range(probs.size/doms[v]):
          start = i*doms[v]
          end = (i+1)*doms[v]
          probs[start:end] /= probs[start:end].sum()
        cpts += [ probs ]
    return cpts

def gen_dbn_cpts(doms, scopes):
    return gen_cpts(np.tile(doms,2), scopes)

def gen_bn_sample(doms, scopes, cpts):
  sample = np.zeros(doms.shape[0], dtype=int)
  for v in range(doms.shape[0]):
    addr = util.calc_address(sample[scopes[v]],doms[scopes[v]])
    dist = cpts[v][addr:addr+doms[v]]
    sample[v] = np.random.choice(doms[v],p=dist)
  return sample

def gen_bn_samples(n, doms, scopes, cpts):
  if(n<0 or doms.shape[0]!=len(cpts)):
    return np.array([])
  samples = np.zeros((n,doms.shape[0]), dtype=int)
  for i in range(n):
    samples[i,:] = gen_bn_sample(doms, scopes, cpts)
  return samples

def gen_dbn_samples(n, doms, scopes, cpts):
  # Check for inconsistencies
  if(n<0 or len(scopes)!=len(cpts) or len(cpts)!=2*doms.shape[0]):
      return np.array([])
  # Initialize parameters
  V = doms.shape[0]
  samples = np.zeros((n,doms.shape[0]), dtype=int)
  samples[0,:] = gen_bn_sample(doms, scopes[0:len(cpts)/2], cpts[0:len(cpts)/2])
  full_doms = np.tile(doms,2)
  # Generate samples
  for i in range(1,n):
    curr_sample = np.hstack((samples[i-1,:],
                              np.zeros(doms.shape[0],dtype=int)))
    for v in range(V,2*V):
      addr = util.calc_address(curr_sample[scopes[v]],full_doms[scopes[v]])
      dist = cpts[v][addr:addr+full_doms[v]]
      curr_sample[v] = np.random.choice(full_doms[v],p=dist)
    samples[i,:] = curr_sample[V:]
  # Return samples
  return samples

doms = np.full(5, 2, dtype=int)
intra_maxlinks = 2
inter_maxlinks = 2
scopes = gen_dbn_scopes(doms, intra_maxlinks, inter_maxlinks)
cpts = gen_dbn_cpts(doms, scopes)
samples = gen_dbn_samples(100, doms, scopes, cpts)
