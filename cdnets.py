import numpy as np
import hmm
import cldbn
import util
import JTree

class CLT:
    def __init__(self):
        self.scopes = []
        self.doms = np.array([])
        self.cpts = []
        self.size = 0
    def learn(self, dset, dom_vals=[]):
        if (dset.ndim!=2):
            return
        dom_vals = dom_vals if (dset.shape[1]==len(dom_vals)) else hmm.extract_all_doms(dset)
        self.doms = util.flatten_dom_vals(dom_vals)
        self.scopes = cldbn.gen_clt_scopes(dset, topo_order_flag=True)
        self.cpts = util.learn_model_params(dset, dom_vals, self.scopes)
        self.size = dset.shape[1]
    def probEvid(self, evid_idx, evid_vals):
        if (evid_idx.size==0):
            return 1.0
        if (evid_idx.ndim!=1 or evid_vals.ndim!=1 or evid_idx.size!=evid_vals.size or \
                (evid_idx<0).sum() or (evid_idx>=self.doms.size).sum() or \
                (self.doms[evid_idx]-evid_vals<=0).sum()):
            return -1.0
        # First, remove evidence from all scopes
        doms = np.array(self.doms)
        cpts = list(self.cpts)
        for i in range(len(self.scopes)):
            cpts[i], _ = util.remove_evid(self.scopes[i], doms[self.scopes[i]], cpts[i],
                                       evid_idx, evid_vals)
        doms[evid_idx] = 1
        # Next, use variable elimination in reverse-topological order to get the answer
        msg_scope = self.scopes[-1][-1:]
        msg_cpt, msg_doms = np.ones(doms[msg_scope].prod()), doms[msg_scope]
        for i in reversed(range(len(self.scopes))):
            scope1, doms1, cpt1 = self.scopes[i], doms[self.scopes[i]], cpts[i]
            marg_scope = np.setdiff1d(np.union1d(scope1,msg_scope), scope1[-1])
            msg_scope, msg_doms, msg_cpt = util.multiply_and_marginalize(scope1, doms1, cpt1,
                                                                        msg_scope, msg_doms, msg_cpt,
                                                                         marg_scope)
        # Finally, return the probability of evidence
        return msg_cpt.sum()
    def LL(self, row=np.array([])):
        if (row.ndim!=1 or row.size != self.size):
            return 1.0
        local_ll = 0.0
        for i in range(len(self.scopes)):
            addr = util.calc_address(row[self.scopes[i]], self.doms[self.scopes[i]])
            if (addr<0 or addr>=self.cpts[i].size):
                return 1.0
            local_ll += np.log(self.cpts[i][addr])
        return local_ll
    def avgLL(self, dset):
        if (dset.ndim!=2 or dset.shape[1] != self.size or dset.shape[0]==0):
            return 1.0
        total_ll = 0.0
        for i in range(dset.shape[0]):
            local_ll = self.LL(dset[i,:])
            if (local_ll>0.0):
                return 1.0
            total_ll += local_ll
        return total_ll / dset.shape[0]

class CNet:
    def __init__(self):
        self.tree = []
        self.size = 0
    def learn(self, dset, max_depth=0, max_clt_thres=1, dom_vals=[]):
        if (dset.ndim!=2 or max_depth<0 or max_clt_thres<0 or (len(dom_vals)%dset.shape[1]) != 0):
            return
        self.size = dset.shape[1]
        dom_vals = dom_vals if (dset.shape[1]==len(dom_vals)) else hmm.extract_all_doms(dset)
        self.tree = self.learnHelper(dset, 0, max_depth, max_clt_thres, dom_vals)
    def learnHelper(self, dset, curr_depth, max_depth, max_clt_thres, dom_vals):
        # Termination condition
        if (curr_depth==max_depth or dset.shape[1]<=max_clt_thres):
            clt = CLT()
            clt.learn(dset, dom_vals)
            return clt
        # Choose variable to split on
        mi_matrix = util.gen_mi_matrix(dset)
        v = mi_matrix.sum(axis=1).argmax()
        # Compute number of branches
        branches = dom_vals[v].size
        children = []
        probs = np.empty(branches, dtype=float)
        new_dom_vals = dom_vals[:v] + dom_vals[v+1:]
        smooth_factor = (dset.shape[0] * 0.001) / branches
        # Recursively learn structures of children
        for c in range(branches):
            c_idx = dset[:,v] == dom_vals[v][c]
            new_dset = np.hstack((dset[c_idx,:v],dset[c_idx,v+1:]))
            probs[c] = ((c_idx.sum()*1.0)+smooth_factor)/(dset.shape[0]+(branches*smooth_factor))
            children += [ self.learnHelper(new_dset,curr_depth+1,max_depth,max_clt_thres,new_dom_vals) ]
        return [ v, probs, children ]
    def probEvid(self, evid_idx, evid_vals):
        if (evid_idx.ndim!=1 or evid_vals.ndim!=1 or evid_idx.size!=evid_vals.size):
            return -1.0
        if (evid_idx.size==0):
            return 1.0
        return self.probEvidHelper(self.tree, evid_idx, evid_vals)
    def probEvidHelper(self, node, evid_idx, evid_vals):
        # Check if current node is a CLT
        if isinstance(node, CLT):
            return node.probEvid(evid_idx, evid_vals)
        # Create new evidence arrays
        e = np.where(evid_idx==node[0])[0][0] if np.where(evid_idx==node[0])[0].size else evid_idx.size
        new_evid_idx = np.hstack((evid_idx[:e],evid_idx[e+1:]))
        new_evid_vals = np.hstack((evid_vals[:e],evid_vals[e+1:]))
        new_evid_idx[new_evid_idx>=node[0]] -= 1
        # If current node is evidence, then condition on the appropriate branch
        if (e<evid_idx.size):
            c = evid_vals[e]
            if (c>=node[1].size):
                return -1.0
            return node[1][c] * self.probEvidHelper(node[2][c], new_evid_idx, new_evid_vals)
        # If not, sum out probability
        prob = 0.0
        for c in range(len(node[2])):
            prob += node[1][c] * self.probEvidHelper(node[2][c], new_evid_idx, new_evid_vals)
        return prob
    def LL(self, row=np.array([])):
        if (row.ndim != 1 or row.size != self.size):
            return 1.0
        return self.LLHelper(self.tree, row)
    def LLHelper(self, node, row):
        # Check if current node is a CLT
        if isinstance(node, CLT):
            return node.LL(row)
        # Condition on the appropriate branch
        c = row[node[0]]
        if (c >= node[1].size):
            return 1.0
        new_row = np.hstack((row[:c],row[c+1:]))
        return np.log(node[1][c]) + self.LLHelper(node[2][c], new_row)
    def avgLL(self, dset):
        if (dset.ndim!=2 or dset.shape[1] != self.size or dset.shape[0]==0):
            return 1.0
        total_ll = 0.0
        for i in range(dset.shape[0]):
            local_ll = self.LL(dset[i,:])
            if (local_ll>0.0):
                return 1.0
            total_ll += local_ll
        return total_ll / dset.shape[0]

class CDNet:
    def __init__(self):
        self.tree = []
        self.x_indices = np.array([])
        self.size = 0
        self.nleaves = 0
    def learn(self, dset, x_indices, max_cond_depth=0, max_depth=0, max_clt_thres=1, dom_vals=[]):
        if (dset.ndim!=2 or max_cond_depth<0 or (len(dom_vals)%dset.shape[1]) != 0 or \
            (x_indices.size>=dset.shape[1]) or (x_indices>=dset.shape[1]).sum()):
            return
        self.x_indices = x_indices
        self.size = dset.shape[1]
        dom_vals = dom_vals if (dset.shape[1]==len(dom_vals)) else hmm.extract_all_doms(dset)
        self.tree = self.learnHelper(dset, x_indices, 0, max_cond_depth, max_depth, max_clt_thres, dom_vals)
    def learnHelper(self, dset, x_indices, curr_depth, max_cond_depth, max_depth, max_clt_thres, dom_vals):
        # Termination condition
        if (curr_depth==max_cond_depth or x_indices.size==0):
            y_indices = np.setdiff1d(np.arange(dset.shape[1]),x_indices)
            new_dset = dset[:,y_indices]
            new_dom_vals = [ dom_val for dom_val in np.array(dom_vals)[y_indices] ]
            cnet = CNet()
            cnet.learn(new_dset, max_depth, max_clt_thres, new_dom_vals)
            self.nleaves += 1
            return cnet
        # Choose variable to split on
        y_indices = np.setdiff1d(np.arange(dset.shape[1]),x_indices)
        mi_matrix = util.gen_mi_matrix_select(dset, y_indices)
        # mi_matrix = util.gen_mi_matrix(dset)
        x = mi_matrix.sum(axis=1)[x_indices].argmax()
        v = x_indices[x]
        # Compute number of branches
        branches = dom_vals[v].size
        children = []
        probs = np.empty(branches, dtype=float)
        new_x_indices = np.hstack((x_indices[:x],x_indices[x+1:]))
        new_x_indices[new_x_indices>=x_indices[x]] -= 1
        new_dom_vals = dom_vals[:v] + dom_vals[v+1:]
        smooth_factor = (max(1,dset.shape[0]) * 0.001) / branches
        # Recursively learn structures of children
        for c in range(branches):
            c_idx = dset[:,v] == dom_vals[v][c]
            new_dset = np.hstack((dset[c_idx,:v],dset[c_idx,v+1:]))
            probs[c] = ((c_idx.sum()*1.0)+smooth_factor)/(dset.shape[0]+(branches*smooth_factor))
            children += [ self.learnHelper(new_dset,new_x_indices,curr_depth+1,max_cond_depth,max_depth,max_clt_thres,new_dom_vals) ]
        return [ v, probs, children ]
    def probEvid(self, evid_idx, evid_vals):
        if (evid_idx.ndim!=1 or evid_vals.ndim!=1 or evid_idx.size!=evid_vals.size or \
                evid_idx.size==0):
            return -1.0
        return self.probEvidHelper(self.tree, self.x_indices, evid_idx, evid_vals)
    def probEvidHelper(self, node, x_indices, evid_idx, evid_vals):
        # Check if current node is a CNet
        if isinstance(node, CNet):
            new_evid_idx = evid_idx[~np.in1d(evid_idx, x_indices)]
            new_evid_idx = np.array([ e-((e>x_indices).sum()) for e in new_evid_idx ])
            new_evid_vals = evid_vals[~np.in1d(evid_idx, x_indices)]
            return node.probEvid(new_evid_idx, new_evid_vals)
        # Create new evidence arrays
        if (np.where(evid_idx == node[0])[0].size == 0):
            return -1.0
        e = np.where(evid_idx == node[0])[0][0]
        new_evid_idx = np.hstack((evid_idx[:e], evid_idx[e + 1:]))
        new_evid_vals = np.hstack((evid_vals[:e], evid_vals[e + 1:]))
        new_evid_idx[new_evid_idx >= node[0]] -= 1
        # Compute new x indices
        x = np.where(x_indices == node[0])[0][0]
        new_x_indices = np.hstack((x_indices[:x], x_indices[x + 1:]))
        new_x_indices[new_x_indices >= x_indices[x]] -= 1
        # Condition on appropriate branch
        c = evid_vals[e]
        if (c >= node[1].size):
            return -1.0
        return self.probEvidHelper(node[2][c], new_x_indices, new_evid_idx, new_evid_vals)
    def multiplyAndMarginalize(self, x_dist, evid_idx=np.array([]),
                                evid_vals=np.array([]), idx_replace=np.array([])):
        if (evid_idx.ndim != 1 or evid_vals.ndim!=1 or evid_idx.size != evid_vals.size or \
                x_dist.size != self.x_indices.size or evid_idx.size > x_dist.size):
            return MixCNet()
        msg = MixCNet()
        msg.size = self.size - self.x_indices.size
        msg.probs = []
        curr_evid_idx, curr_evid_vals = [], []
        all_evid_idx, all_evid_vals = [], []
        idx_replace = np.arange(self.size) if idx_replace.size!=self.size else idx_replace
        self.multiplyAndMarginalizeHelper(self.tree, x_dist, evid_idx, evid_vals,
                                            curr_evid_idx, curr_evid_vals,
                                            all_evid_idx, all_evid_vals, msg.cnets)
        for k in range(len(all_evid_idx)):
            msg.probs += [ x_dist.probEvid(idx_replace[all_evid_idx[k]], all_evid_vals[k]) ]
        msg.probs = np.array(msg.probs)
        msg.probs = msg.probs / msg.probs.sum() if(msg.probs.sum()) else msg.probs
        return msg
    def multiplyAndMarginalizeHelper(self, node, x_dist, evid_idx, evid_vals,
                                     curr_evid_idx, curr_evid_vals,
                                     all_evid_idx, all_evid_vals, cnets):
        # Check if current node is a CNet
        if isinstance(node, CNet):
            all_evid_idx += [ np.array(curr_evid_idx) ]
            all_evid_vals += [ np.array(curr_evid_vals) ]
            cnets += [ node ]
            return
        # Compute current index
        v = node[0] + (np.array(curr_evid_idx)<=node[0]).sum()
        curr_evid_idx += [ v ]
        # Call helper on all children
        for c in range(len(node[2])):
            curr_evid_vals += [ c ]
            if (v not in evid_idx or (v in evid_idx and c==evid_vals[np.where(evid_idx==v)[0][0]])):
                self.multiplyAndMarginalizeHelper(node[2][c], x_dist, evid_idx, evid_vals,
                                              curr_evid_idx, curr_evid_vals,
                                              all_evid_idx, all_evid_vals, cnets)
            curr_evid_vals.pop()
        curr_evid_idx.pop()
    def LL(self, row):
        if (row.ndim != 1 or row.size != self.size):
            return 1.0
        return self.LLHelper(self.tree, self.x_indices, row)
    def LLHelper(self, node, x_indices, row):
        # Check if current node is a CNet
        if isinstance(node, CNet):
            y_indices = np.setdiff1d(np.arange(row.size), x_indices)
            return node.LL(row[y_indices])
        # Condition on the appropriate branch
        c = row[node[0]]
        if (c >= node[1].size):
            return 1.0
        new_row = np.hstack((row[:c], row[c + 1:]))
        # Calculate new x_indices
        x = np.where(x_indices==node[0])[0][0]
        new_x_indices = np.hstack((x_indices[:x], x_indices[x + 1:]))
        new_x_indices[new_x_indices >= x_indices[x]] -= 1
        return self.LLHelper(node[2][c], new_x_indices, new_row)
    def avgLL(self, dset):
        if (dset.ndim != 2 or dset.shape[1] != self.size or dset.shape[0] == 0):
            return 1.0
        total_ll = 0.0
        for i in range(dset.shape[0]):
            local_ll = self.LL(dset[i, :])
            if (local_ll > 0.0):
                return 1.0
            total_ll += local_ll
        return total_ll / dset.shape[0]

class AndCDNet:
    def __init__(self):
        self.cdnets = []
        self.partitions = []
        self.x_indices = np.array([])
        self.size = 0
        self.nleaves = 0
    def learn(self, dset, x_indices, max_cond_depth=0, max_depth=0,
                max_clt_thres=1, max_part_size=1, dom_vals=[]):
        # Validation
        if (dset.ndim!=2 or max_cond_depth<0 or (len(dom_vals)%dset.shape[1]) != 0 or \
            (x_indices.size>=dset.shape[1]) or (x_indices>=dset.shape[1]).sum() or \
            max_part_size<0):
            return
        # Initialization
        all_idx = np.arange(dset.shape[1])
        y_idx = np.setdiff1d(all_idx, x_indices)
        self.size = dset.shape[1]
        self.x_indices = x_indices
        self.partitions = partition_vars(dset[:,y_idx], y_idx, max_part_size)
        dom_vals = dom_vals if (dset.shape[1]==len(dom_vals)) else hmm.extract_all_doms(dset)
        # Learn CDNets
        for partition in self.partitions:
            to_keep_idx = np.union1d(x_indices,partition)
            new_x_indices = np.nonzero(np.in1d(to_keep_idx, x_indices))[0]
            new_dset = dset[:,to_keep_idx]
            new_dom_vals = [ dom_val for dom_val in np.array(dom_vals)[to_keep_idx] ]
            cdnet = CDNet()
            self.cdnets += [ cdnet ]
            # Learn CDNet structure and parameters
            cdnet.learn(new_dset, new_x_indices, max_cond_depth,
                        max_depth, max_clt_thres, new_dom_vals)
            self.nleaves += cdnet.nleaves
    def probEvid(self, evid_idx, evid_vals):
        if (evid_idx.ndim!=1 or evid_vals.ndim!=1 or evid_idx.size!=evid_vals.size or \
                evid_idx.size==0):
            return -1.0
        prob = 1.0
        for p in range(len(self.partitions)):
            partition_scope = np.union1d(self.x_indices, self.partitions[p])
            new_evid_idx = evid_idx[np.in1d(evid_idx, partition_scope)]
            new_evid_idx = np.nonzero(np.in1d(partition_scope, new_evid_idx))[0]
            new_evid_vals = evid_vals[np.in1d(evid_idx, partition_scope)]
            partition_prob = self.cdnets[p].probEvid(new_evid_idx, new_evid_vals)
            if (partition_prob<0.0):
                return -1.0
            prob *= partition_prob
        return prob
    def multiplyAndMarginalize(self, x_dist, evid_idx=np.array([]), evid_vals=np.array([])):
        msg = AndMixCNet()
        msg.size = self.size - self.x_indices.size
        y_indices = np.setdiff1d(np.arange(self.size),self.x_indices)
        msg.partitions = [ np.nonzero(np.in1d(y_indices,partition))[0] for partition in self.partitions ]
        for p in range(len(self.partitions)):
            idx_replace = np.union1d(self.x_indices, self.partitions[p])
            # new_evid_idx = evid_idx[np.in1d(evid_idx, idx_replace)]
            new_evid_idx = np.nonzero(np.in1d(idx_replace, evid_idx))[0]
            new_evid_vals = evid_vals[np.in1d(evid_idx, idx_replace)]
            msg.mixcnets += [ self.cdnets[p].multiplyAndMarginalize(x_dist, new_evid_idx,
                                                                    new_evid_vals, idx_replace) ]
        return msg
    def LL(self, row):
        if (row.ndim != 1 or row.size != self.size):
            return 1.0
        total_ll = 0.0
        for p in range(len(self.partitions)):
            partition_scope = np.union1d(self.x_indices, self.partitions[p])
            new_row = row[partition_scope]
            partition_ll = self.cdnets[p].LL(new_row)
            if (partition_ll>0.0):
                return 1.0
            total_ll += partition_ll
        return total_ll
    def avgLL(self, dset):
        if (dset.ndim != 2 or dset.shape[1] != self.size or dset.shape[0] == 0):
            return 1.0
        total_ll = 0.0
        for i in range(dset.shape[0]):
            local_ll = self.LL(dset[i, :])
            if (local_ll > 0.0):
                return 1.0
            total_ll += local_ll
        return total_ll / dset.shape[0]

class MixCNet:
  def __init__(self):
    self.probs = np.array([])
    self.cnets = []
    self.size = 0
  def isValid(self):
    return self.probs.size==len(self.cnets)
  def probEvid(self, evid_idx, evid_vals):
    if (evid_idx.ndim!=1 or evid_vals.ndim!=1 or evid_idx.size!=evid_vals.size or not self.isValid()):
        return -1.0
    if (evid_idx.size==0):
        return 1.0
    total_prob = 0.0
    for k in range(len(self.probs)):
      prob = self.probs[k] * self.cnets[k].probEvid(evid_idx, evid_vals)
      if (prob<0.0):
        return -1.0
      total_prob += prob
    return total_prob
  def LL(self, row):
      if (row.ndim != 1 or row.size != self.size):
          return 1.0
      prob = self.probEvid(np.arange(row.size), row)
      if (prob>=0.0):
          return np.log(prob)
      return 1.0
  def avgLL(self, dset):
      if (dset.ndim != 2 or dset.shape[1] != self.size or dset.shape[0] == 0):
          return 1.0
      total_ll = 0.0
      for i in range(dset.shape[0]):
          local_ll = self.LL(dset[i, :])
          if (local_ll > 0.0):
              return 1.0
          total_ll += local_ll
      return total_ll / dset.shape[0]

class AndMixCNet:
  def __init__(self):
    self.partitions = []
    self.mixcnets = []
    self.size = 0
  def probEvid(self, evid_idx, evid_vals):
    if (evid_idx.ndim!=1 or evid_vals.ndim!=1 or evid_idx.size!=evid_vals.size):
        return -1.0
    if (evid_idx.size==0):
        return 1.0
    total_prob = 1.0
    for p in range(len(self.partitions)):
      part_evid_idx = np.nonzero(np.in1d(self.partitions[p], evid_idx))[0]
      part_evid_vals = evid_vals[np.in1d(evid_idx, self.partitions[p])]
      prob = self.mixcnets[p].probEvid(part_evid_idx, part_evid_vals)
      if (prob<0.0):
        return -1.0
      total_prob *= prob
    return total_prob
  def LL(self, row):
      if (row.ndim != 1 or row.size != self.size):
          return 1.0
      prob = self.probEvid(np.arange(row.size), row)
      if (prob>=0.0):
          return np.log(prob)
      return 1.0
  def avgLL(self, dset):
      if (dset.ndim != 2 or dset.shape[1] != self.size or dset.shape[0] == 0):
          return 1.0
      total_ll = 0.0
      for i in range(dset.shape[0]):
          local_ll = self.LL(dset[i, :])
          if (local_ll > 0.0):
              return 1.0
          total_ll += local_ll
      return total_ll / dset.shape[0]

def partition_vars(dset, meta_idx=np.array([]), max_part_size=1):
    # Validation
    if (dset.ndim!=2 or meta_idx.ndim!=1):
        return [ np.array([i]) for i in range(dset.shape[1]) ]
    meta_idx = meta_idx if meta_idx.size == dset.shape[1] else np.arange(dset.shape[1])
    if (max_part_size>=dset.shape[1] or max_part_size<=1):
        return [ np.array([i]) for i in meta_idx ]
    # Generate MI Matrix
    mi_matrix = -util.gen_mi_matrix(dset)
    mi_totals = mi_matrix.sum(axis=1)
    # Sort indices in descending order of MI scores
    idx = np.argsort(mi_totals)
    processed_idx = np.array([])
    partitions = []
    # Create partitions
    for i in idx:
        # Check if i has been processed
        if i in processed_idx: continue
        # Create a list of unprocessed indices
        processed_idx = np.union1d(processed_idx, i)
        unprocessed_idx = np.setdiff1d(np.arange(dset.shape[1]),processed_idx)
        unprocessed_scores = np.empty(unprocessed_idx.size)
        # Compute MI scores w.r.t. i for each unprocessed index
        for u in range(unprocessed_idx.size):
            unprocessed_scores[u] = mi_matrix[i][unprocessed_idx[u]]
        # Re-order in descending order
        unprocessed_idx = unprocessed_idx[np.argsort(unprocessed_scores)]
        # Create partition
        partition = np.union1d(i,unprocessed_idx[:max_part_size-1])
        partitions += [ meta_idx[partition] ]
        # Add to processed list
        processed_idx = np.union1d(processed_idx, partition)
    return partitions

def partition_vars1(dset, meta_idx=np.array([]), max_part_size=1):
    if (dset.ndim!=2 or meta_idx.ndim!=1):
        return [ np.array([i]) for i in range(dset.shape[1]) ]
    meta_idx = meta_idx if meta_idx.size == dset.shape[1] else np.arange(dset.shape[1])
    if (max_part_size>=dset.shape[1] or max_part_size<=1):
        return [ np.array([i]) for i in meta_idx ]
    mi_matrix = util.gen_mi_matrix(dset)
    mi_totals = mi_matrix.sum(axis=1)
    idx = np.argsort(mi_totals)
    num_chunks = int(np.ceil((idx.size*1.0)/max_part_size))
    return np.array_split(meta_idx[idx], num_chunks)

def partition_vars2(dset, meta_idx=np.array([]), max_part_size=1):
    # Validation
    if (dset.ndim != 2 or meta_idx.ndim != 1):
        return [np.array([i]) for i in range(dset.shape[1])]
    meta_idx = meta_idx if meta_idx.size == dset.shape[1] else np.arange(dset.shape[1])
    if (max_part_size >= dset.shape[1] or max_part_size <= 1):
        return [np.array([i]) for i in meta_idx]
    # Use BK clusters
    clt_scopes = cldbn.gen_clt_scopes(dset, topo_order_flag=True)
    clt_cpts = [ None ] * len(clt_scopes)
    jtree = JTree.JTree()
    jtree.learnStructure(clt_scopes, clt_cpts)
    return [ meta_idx[partition] for partition in JTree.gen_bk_scopes(jtree, max_part_size) ]

if __name__=="__main__":
    dir_name = "/Users/chiradeep/dev/cdnets/data/japanvowels/"
    file_name = "japanvowels.16.bin"
    train_file_name = file_name + ".train"
    test_file_name = file_name + ".test"
    print("** Loading data")
    train_dset = np.loadtxt(dir_name+"/"+train_file_name, delimiter=",", dtype=int)
    test_dset = np.loadtxt(dir_name + "/" + test_file_name, delimiter=",", dtype=int)
    print("** Learning model")
    andcdnet = AndCDNet()
    andcdnet.learn(train_dset, np.array([0,1,2,3,4]), 3, 1, 1)
    print("** Computing P(evid)")
    evid_idx = np.array([0,1,2,3,4,6])
    evid_vals = np.array([0,0,0,0,0,0])
    print(andcdnet.probEvid(evid_idx, evid_vals))
    evid_vals = np.array([0,0,0,0,0,1])
    print(andcdnet.probEvid(evid_idx, evid_vals))
    print("** Computing LL")
    print(andcdnet.avgLL(test_dset))
    print("Finished Program")
