import numpy as np
import util

def gen_scopes(doms, maxlinks):
    scopes = []
    linkdist = np.exp(np.arange(maxlinks)) / np.exp(np.arange(maxlinks)).sum()
    for v in range(doms.shape[0]):
        nlinks = np.random.choice(maxlinks, p=linkdist) + 1
        curr_scope = np.array([],dtype=int) if v==0 else np.random.choice(v, min(v,nlinks), replace=False)
        scopes += [ np.sort(np.append(curr_scope,v)) ]
    return scopes

def gen_dbn_scopes(doms, intra_maxlinks, inter_maxlinks):
    V = doms.shape[0]
    scopes = gen_scopes(doms, intra_maxlinks)
    intra_linkdist = np.exp(np.arange(intra_maxlinks)) / np.exp(np.arange(intra_maxlinks)).sum()
    inter_linkdist = np.exp(np.arange(inter_maxlinks)) / np.exp(np.arange(inter_maxlinks)).sum()
    for v in range(V):
        intra_nlinks = np.random.choice(intra_maxlinks, p=intra_linkdist) + 1
        inter_nlinks = np.random.choice(inter_maxlinks, p=inter_linkdist) + 1
        curr_scope = np.array([],dtype=int) if v==0 else np.random.choice(v, min(v,intra_nlinks), replace=False) + V
        curr_scope = np.append(curr_scope, np.random.choice(V, min(v,inter_nlinks), replace=False))
        scopes += [ np.sort(np.append(curr_scope,v+V)) ]
    return scopes

def gen_fdbn_scopes(doms):
    V = doms.shape[0]
    scopes = []
    for v in range(2*V):
        scopes += [ np.arange(v+1) ]
    return scopes

def gen_cpts(doms, scopes):
    cpts = []
    for v in range(doms.shape[0]):
        var_doms = doms[scopes[v]]
        # probs = np.exp(np.random.uniform(1,5,var_doms.prod()))
        probs = np.exp(np.random.normal(doms.size,doms.size/2,var_doms.prod()))
        for i in range(probs.size/doms[v]):
          start = i*doms[v]
          end = (i+1)*doms[v]
          probs[start:end] /= probs[start:end].sum()
        cpts += [ probs ]
    return cpts

def gen_dbn_cpts(doms, scopes):
    return gen_cpts(np.tile(doms,2), scopes)

def gen_bn_sample(doms, scopes, cpts):
  sample = np.zeros(doms.shape[0], dtype=int)
  for v in range(doms.shape[0]):
    addr = util.calc_address(sample[scopes[v]],doms[scopes[v]])
    dist = cpts[v][addr:addr+doms[v]]
    sample[v] = np.random.choice(doms[v],p=dist)
  return sample

def gen_bn_samples(n, doms, scopes, cpts):
  if(n<0 or doms.shape[0]!=len(cpts)):
    return np.array([])
  samples = np.zeros((n,doms.shape[0]), dtype=int)
  for i in range(n):
    samples[i,:] = gen_bn_sample(doms, scopes, cpts)
  return samples

def gen_dbn_samples(n, seqs, doms, scopes, cpts):
  # Check for inconsistencies
  if(n<0 or len(scopes)!=len(cpts) or len(cpts)!=2*doms.shape[0] or n!=seqs.sum()):
      return np.array([])
  # Initialize parameters
  samples = np.zeros((n,doms.shape[0]), dtype=int)
  V = doms.shape[0]
  full_doms = np.tile(doms,2)
  cum_seqs = np.append(0,seqs.cumsum())
  # Generate samples
  for s in range(cum_seqs.shape[0]-1):
      samples[cum_seqs[s],:] = gen_bn_sample(doms, scopes[0:len(cpts)/2], cpts[0:len(cpts)/2])
      for i in range(cum_seqs[s]+1,cum_seqs[s+1]):
          curr_sample = np.hstack((samples[i-1,:],
                                    np.zeros(doms.shape[0],dtype=int)))
          for v in range(V,2*V):
              addr = util.calc_address(curr_sample[scopes[v]],full_doms[scopes[v]])
              dist = cpts[v][addr:addr+full_doms[v]]
              curr_sample[v] = np.random.choice(full_doms[v],p=dist)
          samples[i,:] = curr_sample[V:]
  # Return samples
  return samples

# [EXPONENTIAL] Convert bn to a message table
def convert_cpts_to_msg(doms, scopes, cpts):
  msg = np.zeros(doms.prod())
  # Check for validity
  if (doms.shape[0] != len(scopes) or len(scopes)!=len(cpts)):
    return msg
  # Compute msg
  V = doms.shape[0]
  curr_assign = np.zeros(V, dtype=int)
  for i in range(msg.size):
    prob = 1.0
    for v in range(V):
      addr = util.calc_address(curr_assign[scopes[v]], doms[scopes[v]])
      prob *= cpts[v][addr]
    msg[i] += prob
    util.go_to_next_assign(curr_assign, doms)
  # Return msg
  return msg

# Convert a message table to a set of CPTs
def convert_msg_to_cpts(msg, doms, scopes):
  # Check for validity
  if (doms.prod()!=msg.shape[0] or doms.shape[0] != len(scopes)):
    return []
  # Initialize variables
  V = doms.shape[0]
  full_scope = np.arange(V, dtype=int)
  cpts = []
  # Generate each CPT in scope
  for v in range(V):
    curr_assign = np.zeros(scopes[v].shape[0], dtype=int)
    cpt = np.zeros(doms[scopes[v]].prod())
    # Iterate through all entries in CPT
    for i in range(cpt.shape[0]):
      curr_full_assign = np.zeros(V, dtype=int)
      curr_full_assign[scopes[v]] = curr_assign
      sum_out_scope = np.setdiff1d(full_scope,scopes[v])
      # Sum out unneeded variables
      for j in range(doms[sum_out_scope].prod()):
        cpt[i] += msg[util.calc_address(curr_full_assign, doms)]
        curr_full_assign[sum_out_scope] = util.calc_next_assign(curr_full_assign[sum_out_scope], doms[sum_out_scope])
      # Normalize conditional distribution
      if ((i+1)%doms[v]==0 and cpt[i-doms[v]+1:i+1].sum()>0.0):
        cpt[i-doms[v]+1:i+1] /= cpt[i-doms[v]+1:i+1].sum()
      util.go_to_next_assign(curr_assign, doms[scopes[v]])
    cpts += [ cpt ]
  # Return CPTs
  return cpts

# [EXPONENTIAL] Calculate message for current time slice given previous message
def calc_next_msg(prev_msg, doms_t, scopes_t, cpts_t):
  # Check for validity
  if (doms_t.shape[0] != len(scopes_t) or len(scopes_t)!=len(cpts_t) or \
      prev_msg.shape[0] != doms_t.prod()):
      return np.zeros(doms_t.prod())
  # Initialize variables
  V = doms_t.shape[0]
  full_doms = np.tile(doms_t,2)
  new_msg = np.zeros(doms_t.prod())
  curr_assign = np.zeros(V*2, dtype=int)
  # Calculate each new entry in joint table
  for i in range(new_msg.size):
    for j in range(new_msg.size):
      addr1 = util.calc_address(curr_assign[:V], doms_t)
      prob = prev_msg[addr1]
      for v in range(V):
        addr2 = util.calc_address(curr_assign[scopes_t[v]], full_doms[scopes_t[v]])
        prob *= cpts_t[v][addr2]
      new_msg[i] += prob
      util.go_to_next_assign(curr_assign[:V], doms_t)
    util.go_to_next_assign(curr_assign[V:], doms_t)
  # Return new message
  return new_msg

# [EXPONENTIAL] Calculate exact messages for a DBN for max_seq_size time slices
def calc_exact_dbn_msgs(max_seq_size, doms, scopes, cpts):
  # Check for validity
  if (max_seq_size<1 or 2*doms.shape[0] != len(scopes) or len(scopes) != len(cpts)):
    return [ ]
  # Initialize variables
  V = doms.shape[0]
  msgs = [ convert_cpts_to_msg(doms, scopes[:V], cpts[:V]) ]
  # Propagate message through the DBN
  for i in range(1,max_seq_size):
    if not i%50: print("*** Computing exact DBN message for t="+str(i+1))
    msgs += [ calc_next_msg(msgs[i-1], doms, scopes[V:], cpts[V:]) ]
  # Return all calculated messages
  return msgs

# [EXPONENTIAL] Compute average log-likelihood of dataset w.r.t DBN
def dbn_avg_LL(dset, seqs, doms, scopes, cpts):
  # Check for validity
  if (dset.shape[0] != seqs.sum() or doms.shape[0]*2 != len(scopes) or \
      len(scopes)!=len(cpts)):
    return 1.0
  # Pre-compute messages
  # max_seq_size = seqs.max()
  # msgs = calc_exact_dbn_msgs(max_seq_size, doms, scopes, cpts)
  # if (len(msgs) != max_seq_size):
  #  return 1.0
  # Compute LL for each data point
  total_ll = 0.0
  V = doms.shape[0]
  cum_seqs = np.append(0, seqs[:-1]).cumsum()
  full_doms = np.tile(doms, 2)
  for s in range(seqs.shape[0]):
    curr_data = np.zeros(V*2, dtype=int)
    for i in range(cum_seqs[s], cum_seqs[s]+seqs[s]):
        start = V if i > cum_seqs[s] else 0
        curr_data[start:start+V] = dset[i]
        for v in range(V):
            addr = util.calc_address(curr_data[scopes[start+v]], full_doms[scopes[start+v]])
            total_ll += np.log(cpts[start+v][addr])
        curr_data[:V] = dset[i]
  # Return average LL
  return total_ll / dset.shape[0]

# Train and test a CLDBN from a training and test set
def dbn_inference(dir_name, file_name, dom_vals=[]):
    # Initialize variables
    infile_path = dir_name + '/' + file_name
    # Load test dataset
    test_dset = np.loadtxt(infile_path + ".test", delimiter=',', dtype=int)
    test_seqs = np.loadtxt(infile_path + ".test.seq", delimiter=',', ndmin=1, dtype=int)
    # Load model
    doms, scopes, cpts = util.load_dbn_model(dir_name, file_name)
    # Return average log-likelihood
    return dbn_avg_LL(test_dset, test_seqs, doms, scopes, cpts)
