import numpy as np
import util

# Generate CPTs for a set of domains
def gen_cpts(doms):
  cpts = []
  cum_doms = np.cumprod(doms)
  prev_doms = np.append(doms[0],cum_doms[:-1])
  for d in range(cum_doms.shape[0]):
    probs = np.exp(np.random.uniform(1,5,cum_doms[d]))
    for i in range(probs.size/doms[d]):
      start = i*doms[d]
      end = (i+1)*doms[d]
      probs[start:end] /= probs[start:end].sum()
    cpts += [ probs ]
  return cpts

# Generate CPTs for a DBN
def gen_dbn_cpts(doms):
  return gen_cpts(np.tile(doms,2))

# Sample from fully-connected network
def gen_bn_sample(doms, cpts):
  sample = np.zeros(doms.shape[0], dtype=int)
  for v in range(doms.shape[0]):
    addr = util.calc_address(sample[:v+1],doms[:v+1])
    dist = cpts[v][addr:addr+doms[v]]
    sample[v] = np.random.choice(doms[v],p=dist)
  return sample

# Generate multiple samples from fully-connected network
def gen_bn_samples(n, doms, cpts):
  if(n<0 or doms.shape[0]!=len(cpts)):
    return np.array([])
  samples = np.zeros((n,doms.shape[0]), dtype=int)
  for i in range(n):
    samples[i,:] = gen_bn_sample(doms, cpts)
  return samples

# Generate multiple samples from fully-connected DBN
def gen_dbn_samples(n, doms, cpts):
  # Check for inconsistencies
  if(n<0 or len(cpts)!=2*doms.shape[0]):
    return np.array([])
  # Initialize parameters
  samples = np.zeros((n,doms.shape[0]), dtype=int)
  samples[0,:] = gen_bn_sample(doms, cpts[0:len(cpts)/2])
  full_doms = np.tile(doms,2)
  # Generate samples
  for i in range(1,n):
    curr_sample = np.hstack((samples[i-1,:],
                              np.zeros(doms.shape[0],dtype=int)))
    for v in range(doms.shape[0]):
      limit = doms.shape[0]+v
      addr = util.calc_address(curr_sample[:limit+1],full_doms[:limit+1])
      dist = cpts[limit][addr:addr+doms[v]]
      curr_sample[limit] = np.random.choice(doms[v],p=dist)
    samples[i,:] = curr_sample[doms.shape[0]:]
  return samples
