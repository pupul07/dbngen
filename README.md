
# README #
This toolkit provides a collection of machine learning techniques based on probabilistic graphical models that can be used to model temporal or sequential data. It includes the following modules:

1. __dbngen.py:__ Generates training and test data sets sampled from synthetic DBNs.
2. __hmm.py:__ Computes log-likelihood or conditional log-likelihood scores of a sequential dataset using _n_ independent Markov chains where _n_ is the total number of variables/features in the dataset.
3. __cldbn.py:__ Computes log-likelihood scores of a sequential dataset using a Chow-Liu DBN.
4. __AndDCN.py:__ Computes log-likelihood or conditional log-likelihood scores of a sequential dataset using a dynamic cutset network whose transition distribution is modeled using an AndCDNet.
5. __DCCN.py:__ Computes log-likelihood or conditional log-likelihood scores of a sequential dataset using a dynamic cutset network whose transition distribution is modeled using a CCN (Conditional Cutset Network).

## Dependencies
1. Install networkx: `python -m pip install networkx`

## Data Format
Each dataset needs to consist of the following files:
1. _data.train:_ Training file containing all training sequences
2. _data.test:_ Test file containing all test sequences
3. _data.train.seq:_ Comma-delimited file comprising of size of each train sequence
4. _data.test.seq:_ Comma-delimited file comprising of size of each test sequence
5. _data.doms:_ File specifying all domain values

_Example of sequence file with 3 sequences of size 3, 5 and 7 respectively:_
3, 5, 7

_Example of domain file with 2 variables one with domain {3,7} and the other with domain {1,8,9}:_
2
3, 7
1, 8, 9

## dbngen.py
This module can be used to generate synthetic data using high-treewidth artificially generated DBNs. Requires _networkx_.

### Usage:
_python __dbngen.py__ \<path\> \<name\> \<num-samples\> \<max-seq-size\> \<domains\> \<type\> [ flags ]_

### Arguments:
\<path\>: Path to output training and test datasets
\<name\>: Name of the new dataset
\<num-samples\>: Number of training samples to generate
\<max-seq-size\>: Maximum length of an individual sequence
\<domains\>: Comma-delimited array of domain values
\<type\>: f (Fully-connected DBN), p (Partially-connected DBN)

### Flags:
-D \<n\>: Duplicates <domains> n times
-s \<n\>: Random seed
-i \<n\>: Maximum intra-slice in-degree (required for PDBN)
-I \<n\>: Maximum inter-slice in-degree (required for PDBN)
-l \<path-to-file\>: Load model from given path

### Usage Example:
1. **Sample 1000 points from a fully-connected DBN over 2 variables having domains 2 and 3 with seed 2020:**
	`python dbngen.py "path/to/data" model1 1000 2,3 f -s 2020`
2. **Sample 100 points from a DBN with maximum i=2 and maximum I=3 over 5 binary variables:**
	`python dbngen.py "path/to/data" model2 1000 2 p -i 2 -I 3 -D 5`
3. **Load model radar.dbn and draw 100 samples from it:**
	`python dbngen.py "path/to/data" model3 100 2 p -l "path/to/radar"`
	
## hmm.py
This module trains a generative HMM model on a sequential dataset and computes the log-likelihood or conditional log-likelihood score. This is just _n_ independent Markov chains on each variable.

### Usage:
_python __hmm.py__ \<dir-path\> \<file-name\>_

### Arguments:
_\<dir-path\>:_ Path to directory that contains the datasets
_\<file-name\>:_ Name of the dataset

### Usage Example:
`python hmm.py ../cdnets/data/japanvowels japanvowels.8.bin 18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35`

## cldbn.py
This module trains a Chow-Liu DBN on a sequential dataset and computes the log-likelihood score for it. A Chow-Liu tree is trained on a single time slice and the interface edges are drawn in the forward direction connecting the same variable between time slices.

### Usage:
_python __cldbn.py__ \<dir-path\> \<file-name\>_

### Arguments:
_\<dir-path\>:_ Path to directory that contains the datasets
_\<file-name\>:_ Name of the dataset

### Usage Example:
`python cldbn.py ../cdnets/data/japanvowels japanvowels.8.bin`

## AndDCN.py
This module uses a Dynamic Cutset Network (DCN) to model a sequential dataset where the prior distribution is modeled using a regular cutset network (CN) and the transition distribution is modeled using a cutset decision network (CDNet). The structural constraints imposed by the CDNet makes exact inference tractable on this model. Further, a single AND node is used to model independencies by introducing decomposition into the model.

### Usage:
_python __AndDCN.py__ \<dir-path\> \<file-name\> \<prior-depth\> \<prior-thres\> \<trans-cond-depth\> \<trans-depth\> \<trans-thres\> \<max-part-size\> [ \<evid-idx\>]_

### Arguments:
_\<dir-path\>:_	Path to directory that contains the datasets
_\<file-name\>:_	Name of the dataset
_\<prior-depth\>:_	Maximum depth of prior cutset network
_\<prior-thres\>:_	CLT variable threshold of prior cutset network
_\<trans-cond-depth\>:_	Maximum conditional depth of transition CDNet
_\<trans-depth\>:_	Maximum depth of transition CDNet
_\<trans-thres\>:_	CLT variable threshold of transition CDNet
_\<max-part-size\>:_	Size of maximum partition for AndCDNet
_\<evid-idx\>:_	Evidence indices for computing CLL (optional)

### Usage Example:
`python AndDCN.py ../cdnets/data/japanvowels/ japanvowels.8.bin 2 1 2 1 1 2 18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35`

## DCCN.py
This module uses a Dynamic Cutset Network (DCN) to model a sequential dataset where the prior distribution is modeled using a regular cutset network (CN) and the transition distribution is modeled using a conditional cutset network (CCN). The fact that the conditional probabilities can be modeled as non-linear functions using neural networks makes this a powerful model. However, the disadvantage of this approach is that exact inference is no longer tractable on the joint model. Approximate inference is performed using weighted particle filtering.

### Usage:
_python __DCCN.py__ \<dir-path\> \<file-name\> \<prior-depth\> \<trans-depth\> \<function\> \<alpha\> [ \<evid-idx\> \<K\> ]_

### Arguments:
_\<dir-path\>:_	Path to directory that contains the datasets
_\<file-name\>:_	Name of the dataset
_\<prior-depth\>:_	Maximum depth of prior cutset network
_\<trans-depth\>:_	Maximum depth of transition conditional cutset network (CCN)
_\<function\>:_	Function to be used for CCN (LR or NN)
_\<alpha\>:_	Hyperparameter for neural network
_\<evid-idx\>:_	Evidence indices for computing CLL (optional)
_\<K\>:_	Number of particles for particle filtering (optional; default: 10)

### Usage Example:
`python DCCN.py ../cdnets/data/japanvowels/ japanvowels.8.bin 2 2 LR 0.01 18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35 15`