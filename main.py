import numpy as np

import time
import sys
import os
import fdbn
import pdbn
import util

usage_str = """Usage:
======
python main.py <path> <name> <num-samples> <domains> <type> [ flags ]

Arguments:
==========
<path>:\t\t\tPath to output training and test datasets
<name>:\t\t\tName of the new dataset
<num-samples>:\t\tNumber of training samples to generate
<domains>:\t\tComma-delimited array of domain values
<type>:\t\t\tf (Fully-connected DBN)
\t\t\tp (Partially-connected DBN)

Flags:
======
-D <n>:\t\t\tDuplicates <domains> n times
-s <n>:\t\t\tRandom seed
-i <n>:\t\t\tMaximum intra-slice in-degree (required for PDBN)
-I <n>:\t\t\tMaximum inter-slice in-degree (required for PDBN)
-l <path-to-file>:\tLoad model from given path"""

# Initialize variables
flag_vals = { "-D": "1",
                "-s": str(int(time.time()*1000000)),
                "-i": "-1",
                "-I": "-1",
                "-l": "" }

# Show usage string if arguments not present
if (len(sys.argv)<6):
    print(usage_str)
    sys.exit(1)

# Process Flags
curr_index = 6
while (curr_index<len(sys.argv)):
    flag = sys.argv[curr_index].strip()
    if flag not in flag_vals or curr_index+1==len(sys.argv):
        curr_index += 1
        continue
    flag_vals[flag] = sys.argv[curr_index+1].strip()
    curr_index += 2

# Read in arguments
path = sys.argv[1].strip()
name = sys.argv[2].strip()
n = int(sys.argv[3])
doms = np.tile(np.fromstring(sys.argv[4], dtype=int, sep=","),int(flag_vals["-D"]))
type = sys.argv[5].strip()

# Generate temporal data
print("=====================================")
print("TEMPORAL DATA GENERATOR")
print("=====================================")
print("Arguments")
print("=========")
print("path: " + path)
print("name: " + name)
print("n: " + str(n))
print("domains: " + np.array2string(doms, separator=","))
print("type: " + type)
print("seed: " + flag_vals["-s"] + "\n")

# Generate samples
print("Sampling")
print("========")
np.random.seed(int(flag_vals["-s"]))
if (type=="p" or flag_vals["-l"]):
    if (flag_vals["-l"]):
        print("Loading model from " + flag_vals["-l"])
        doms, scopes, cpts = util.load_dbn_model(os.path.dirname(flag_vals["-l"]), os.path.basename(flag_vals["-l"]))
    else:
        intra_maxlinks, inter_maxlinks = int(flag_vals["-i"]), int(flag_vals["-I"])
        if min(intra_maxlinks, inter_maxlinks) < 0:
            print("Error! Please provide -i and -I flags for pDBNs!")
            sys.exit(1)
        print("Training pDBN (i="+flag_vals["-i"]+", I="+flag_vals["-I"]+")")
        scopes = pdbn.gen_dbn_scopes(doms, intra_maxlinks, inter_maxlinks)
        cpts = pdbn.gen_dbn_cpts(doms, scopes)
    model_name = name + ".p." + flag_vals["-i"] + "." + flag_vals["-I"]
    outfile_path = path + "/" + model_name
    print("Sampling training set from pDBN (n="+str(n)+")")
    train_samples = pdbn.gen_dbn_samples(n, doms, scopes, cpts)
    print("Sampling test set from pDBN (n="+str(n/10)+")")
    test_samples = pdbn.gen_dbn_samples(n/10, doms, scopes, cpts)
else:
    model_name = name + ".f." + str(doms.size)
    outfile_path = path + "/" + model_name
    print("Training fDBN")
    cpts = fdbn.gen_dbn_cpts(doms)
    print("Sampling training set from fDBN (n="+str(n)+")")
    train_samples = fdbn.gen_dbn_samples(n, doms, cpts)
    print("Sampling test set from fDBN (n="+str(n/10)+")")
    test_samples = fdbn.gen_dbn_samples(n/10, doms, cpts)

# Save data to disk
print("Saving model to disk")
util.save_dbn_model(path, model_name, doms, scopes, cpts)
util.draw_dbn_model(path, model_name, doms, scopes)
print("Saving training samples to disk")
np.savetxt(outfile_path+".train", train_samples, fmt="%i", delimiter=",")
print("Saving test samples to disk")
np.savetxt(outfile_path+".test", test_samples, fmt="%i", delimiter=",")
print("Finished program!")

# python main.py "/Users/chiradeep/dev/cdnets/data" synth 10000 2 p -i 2 -I 2 -D 5
