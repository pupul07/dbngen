import numpy as np
import util
import JTree
import cldbn
import hmm
import dbn

# def convert_cldbn_to_ccldbn(scopes, doms, cpts, cluster_scopes):
#   # Validation
#   if (len(scopes)!=len(doms) or len(doms)!=len(cpts) or \
#       sum([ len(cluster_scope) for cluster_scope in cluster_scopes]) != (len(scopes)/2)):
#     return scopes, doms, cpts
#   # Initialization
#   scopes = list(scopes)
#   doms = np.array(doms)
#   cpts = list(cpts)
#   V = len(scopes)/2
#   var_idx_dict = dict(zip([ scope[-1] for scope in scopes ],range(len(scopes))))
#   t_cluster_scopes = [ cluster_scope + V for cluster_scope in cluster_scopes ]
#   # Process each cluster
#   for t in range(1,len(t_cluster_scopes)):
#     # Separate into sum-out and non-sum-out variables
#     cluster_root_scope = scopes[var_idx_dict[t_cluster_scopes[t][0]]]
#     sum_out_vars = np.hstack((t_cluster_scopes[t-1]-V,t_cluster_scopes[t-1]))
#     non_sum_out_vars = np.append(np.setdiff1d(cluster_root_scope[:-1], sum_out_vars), cluster_root_scope[-1])
#     # Create mappings from variables to assignment vectors
#     sum_out_var_dict = dict(zip(sum_out_vars,np.arange(sum_out_vars.size)))
#     non_sum_out_var_dict = dict(zip(non_sum_out_vars,np.arange(non_sum_out_vars.size)))
#     # Isolate domains
#     sum_out_doms = np.array([ doms[var_idx_dict[sum_out_var]] for sum_out_var in sum_out_vars ])
#     non_sum_out_doms = np.array([ doms[var_idx_dict[non_sum_out_var]] for non_sum_out_var in non_sum_out_vars ])
#     root_doms = np.array([ sum_out_doms[sum_out_var_dict[var]] if (var in sum_out_vars) \
#                             else non_sum_out_doms[non_sum_out_var_dict[var]] \
#                             for var in cluster_root_scope ])
#     # Isolate scopes and CPTs
#     sum_out_scopes = [ scopes[var_idx_dict[sum_out_var]] for sum_out_var in sum_out_vars ]
#     new_scope = non_sum_out_vars
#     sum_out_cpts = [ cpts[var_idx_dict[sum_out_var]] for sum_out_var in sum_out_vars ]
#     root_cpt = cpts[var_idx_dict[t_cluster_scopes[t][0]]]
#     new_cpt = np.zeros(non_sum_out_doms.prod())
#     # Create assignment vectors
#     sum_out_assign = np.zeros(len(sum_out_vars), dtype=int)
#     non_sum_out_assign = np.zeros(len(non_sum_out_vars), dtype=int)
#     # Calculate new CPT
#     for i in range(sum_out_doms.prod()):
#       prob = 1.0
#       # Compute joint probability over sum-out variables
#       for s in range(len(sum_out_vars)):
#         sum_out_scope = sum_out_scopes[s]
#         sum_out_cpt = sum_out_cpts[s]
#         sum_out_vals = np.array([ sum_out_assign[sum_out_var_dict[var]] for var in sum_out_scope ])
#         sum_out_local_doms = np.array([ sum_out_doms[sum_out_var_dict[var]] for var in sum_out_scope ])
#         sum_out_addr = util.calc_address(sum_out_vals, sum_out_local_doms)
#         prob *= sum_out_cpt[sum_out_addr]
#       # Multiply with root probability and add to new cpt
#       for j in range(non_sum_out_doms.prod()):
#         root_vals = np.array([ sum_out_assign[sum_out_var_dict[var]] if (var in sum_out_vars) \
#                           else non_sum_out_assign[non_sum_out_var_dict[var]] \
#                           for var in cluster_root_scope ])
#         root_addr = util.calc_address(root_vals, root_doms)
#         new_addr = util.calc_address(non_sum_out_assign, non_sum_out_doms)
#         new_cpt[new_addr] += root_cpt[root_addr] * prob
#         util.go_to_next_assign(non_sum_out_assign, non_sum_out_doms)
#       util.go_to_next_assign(sum_out_assign, sum_out_doms)
#     # Substitute new CPT with old one
#     scopes[var_idx_dict[t_cluster_scopes[t][0]]] = new_scope
#     cpts[var_idx_dict[t_cluster_scopes[t][0]]] = new_cpt
#   # Return new model
#   return scopes, doms, cpts

def convert_to_ccldbn_scopes(scopes, cluster_scopes, keep_parent_edges=False):
    # Validation
    if (sum([ len(cluster_scope) for cluster_scope in cluster_scopes]) != (len(scopes)/2)):
        return scopes
    # Initialization
    scopes = list(scopes)
    V = len(scopes)/2
    var_idx_dict = dict(zip([ scope[-1] for scope in scopes ],range(len(scopes))))
    t_cluster_scopes = [ cluster_scope + V for cluster_scope in cluster_scopes ]
    p_vars = np.array([ scope[-1] for scope in scopes[:V] ])
    t_vars = np.array([ scope[-1] for scope in scopes[V:] ])
    # Process each cluster
    for cluster_scope in t_cluster_scopes[1:]:
        # Get head of cluster
        cluster_head = cluster_scope[0]
        # Get index of cluster_head
        h = var_idx_dict[cluster_head]
        # Change scope
        scopes[h] = np.append(np.setdiff1d(scopes[h][:-1],t_vars), cluster_head)
        # Also change scope of prior if keep parent edges flag is false
        if not keep_parent_edges:
            scopes[h-V] = np.append(np.setdiff1d(scopes[h-V][:-1],p_vars), cluster_head-V)
    # Return new scopes
    return scopes

def convert_to_ccldbn_scopes2(scopes, cluster_scopes, keep_parent_edges=False):
    # Validation
    if (sum([len(cluster_scope) for cluster_scope in cluster_scopes]) != (len(scopes) / 2)):
        return scopes
    # Initialization
    scopes = list(scopes)
    V = len(scopes) / 2
    # Process each cluster
    for cluster_scope in cluster_scopes:
        # Remove all non-cluster dependencies from each variable
        mega_cluster_scope = np.hstack((cluster_scope, cluster_scope+V))
        for v in cluster_scope:
            scopes[V+v] = np.append(np.intersect1d(scopes[V+v][:-1],mega_cluster_scope),V+v)
            if not keep_parent_edges:
                scopes[v] = np.append(np.intersect1d(scopes[v][:-1], cluster_scope), v)
    # Return scopes
    return scopes

# Learn parameters given actual domain values and scopes
def learn_ccldbn(dset, seqs, dom_vals=[]):
    # Return empty model if not valid
    if (dset.ndim!=2 or dset.shape[0] != seqs.sum()):
        return np.array([]), [], []
    # Generate transition dataset
    transition_dset = hmm.create_transition_dset(dset, seqs)
    # Generate dom_vals, if not provided
    dom_vals = dom_vals if (dset.shape[1]==len(dom_vals)) else hmm.extract_all_doms(dset)
    # Flatten domains
    doms = util.flatten_dom_vals(dom_vals)
    # Generate CLDBN scopes
    scopes = cldbn.gen_cldbn_scopes(dset)
    topo_scopes = cldbn.gen_cldbn_scopes(dset, topo_order_flag=True)
    cpts = dbn.gen_cpts(doms*2, topo_scopes)
    # Use CLDBN scopes to generate CCLDBN scopes
    V = len(dom_vals)
    jtree = JTree.JTree()
    jtree.learnStructure(topo_scopes[:V], cpts[:V])
    bk_scopes = JTree.gen_bk_scopes(jtree, 3)
    scopes = convert_to_ccldbn_scopes2(scopes, bk_scopes, True)
    # Generate CPTs
    cpts = util.learn_model_params(transition_dset, dom_vals*2, scopes)
    cpts[:V] = util.learn_model_params(dset, dom_vals, scopes[:V])
    # Return model as doms, scopes and cpts
    return doms, scopes, cpts

# Find average log-likelihood as test dataset
def ccldbn_avg_LL(dset, seqs, doms, scopes, cpts):
    return dbn.dbn_avg_LL(dset, seqs, doms, scopes, cpts)

# Train and test a CCLDBN from a training and test set
def ccldbn_inference(dir_name, file_name, dom_vals=[]):
    # Initialize variables
    infile_path = dir_name + '/' + file_name
    # Load datasets
    train_dset = np.loadtxt(infile_path + ".train", delimiter=',', dtype=int)
    test_dset = np.loadtxt(infile_path + ".test", delimiter=',', dtype=int)
    train_seqs = np.loadtxt(infile_path + ".train.seq", delimiter=',', ndmin=1, dtype=int)
    test_seqs = np.loadtxt(infile_path + ".test.seq", delimiter=',', ndmin=1, dtype=int)
    # Train model
    dom_vals = hmm.extract_all_doms(train_dset) if (len(dom_vals)!=train_dset.shape[1]) else dom_vals
    doms, scopes, cpts = learn_ccldbn(train_dset, train_seqs, dom_vals)
    # Return average log-likelihood
    new_test_dset = util.dom_normalize_dset(test_dset, dom_vals)
    return ccldbn_avg_LL(new_test_dset, test_seqs, doms, scopes, cpts)