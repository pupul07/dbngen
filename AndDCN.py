import numpy as np
import hmm
import cdnets
import util
import sys

usage_str = """Usage:
======
python AndDCN.py <dir-path> <file-name> <prior-depth> <prior-thres>
                <trans-cond-depth> <trans-depth> <trans-thres> <max-part-size>

Arguments:
==========
<dir-path>:\tPath to directory that contains the datasets
<file-name>:\tName of the dataset
<prior-depth>:\tMaximum depth of prior cutset network
<prior-thres>:\tCLT variable threshold of prior cutset network
<trans-cond-depth>:\tMaximum conditional depth of transition CDNet
<trans-depth>:\tMaximum depth of transition CDNet
<trans-thres>:\tCLT variable threshold of transition CDNet
<max-part-size>:\tSize of maximum partition for AndCDNet"""

class DCN:
    def __init__(self):
        self.prior_model = None
        self.transition_model = None
        self.size = 0
    def learn(self, dset, seqs, prior_max_depth=0, prior_max_clt_thres=1, max_cond_depth=0,
                max_depth=0, max_clt_thres=1, max_part_size=1, dom_vals=[]):
        # Validation
        if (dset.ndim!=2 or dset.shape[0]==0 or seqs.ndim!=1 or dset.shape[0] != seqs.sum() or max_part_size<0 ):
            return
        # Initialize models
        self.prior_model = cdnets.CNet()
        self.transition_model = cdnets.AndCDNet()
        self.size = dset.shape[1]
        dom_vals = dom_vals if (dset.shape[1]==len(dom_vals)) else hmm.extract_all_doms(dset)
        # Learn parameters
        print("** Learning prior model")
        self.prior_model.learn(dset, prior_max_depth, prior_max_clt_thres, dom_vals)
        print("** Learning transition model")
        transition_dset = hmm.create_transition_dset(dset, seqs)
        x_indices = np.arange(dset.shape[1])
        self.transition_model.learn(transition_dset, x_indices, max_cond_depth,
                                    max_depth, max_clt_thres, max_part_size, 2*dom_vals)
    def avgLL(self, dset, seqs, evid_idx=np.array([])):
        if (dset.ndim!=2 or dset.shape[0]==0 or seqs.ndim!=1 or dset.shape[0] != seqs.sum()):
            return 1.0
        total_ll = 0.0
        cum_seqs = np.append(0,seqs[:-1]).cumsum()
        for s in range(seqs.shape[0]):
            local_ll = self.prior_model.LL(dset[cum_seqs[s]])
            local_ell = 0.0
            if (evid_idx.size):
                local_ell = np.log(self.prior_model.probEvid(evid_idx, dset[cum_seqs[s], evid_idx]))
            if (local_ll>0.0 or local_ell>0.0):
                return 1.0
            total_ll += (local_ll-local_ell)
            prev_msg = self.prior_model
            for i in range(cum_seqs[s]+1, cum_seqs[s]+seqs[s]):
                local_ll = self.transition_model.LL(np.hstack((dset[i-1],dset[i])))
                local_ell = 0.0
                if (evid_idx.size):
                    msg = self.transition_model.multiplyAndMarginalize(prev_msg, evid_idx, dset[i-1, evid_idx])
                    local_ell = np.log(msg.probEvid(evid_idx, dset[i, evid_idx]))
                    prev_msg = msg
                if (local_ll>0.0 or local_ell>0.0):
                    return 1.0
                total_ll += (local_ll-local_ell)
        # Return average LL
        return total_ll / dset.shape[0]

if __name__=="__main__":
    # Check if arguments are present
    if (len(sys.argv) < 9):
        print(usage_str)
        sys.exit(1)
    # Read in arguments
    dir_name = sys.argv[1]
    file_name = sys.argv[2]
    max_depth_prior = int(sys.argv[3])
    clt_thres_prior = int(sys.argv[4])
    max_cond_depth = int(sys.argv[5])
    max_depth = int(sys.argv[6])
    clt_thres = int(sys.argv[7])
    max_part_size = int(sys.argv[8])
    evid_idx = np.fromstring(sys.argv[9], sep=",", dtype=int) if (len(sys.argv)>9) else np.array([])
    print("==========================")
    print("DCN Inference")
    print("==========================")
    print("Directory: " + dir_name)
    print("File Name: " + file_name)
    print("Max Depth (Prior): " + str(max_depth_prior))
    print("CLT Threshold (Prior): " + str(clt_thres_prior))
    print("Max Conditional Depth (Transition): " + str(max_cond_depth))
    print("Max Depth (Transition): " + str(max_depth))
    print("CLT Threshold (Transition): " + str(clt_thres))
    print("Max Partition Size: " + str(max_part_size))
    print("Evidence Indices: " + str(evid_idx))
    # Load datasets
    print("** Loading data")
    infile_path = dir_name + '/' + file_name
    train_dset = np.loadtxt(infile_path + ".train", delimiter=',', dtype=int)
    test_dset = np.loadtxt(infile_path + ".test", delimiter=',', dtype=int)
    train_seqs = np.loadtxt(infile_path + ".train.seq", delimiter=',', ndmin=1, dtype=int)
    test_seqs = np.loadtxt(infile_path + ".test.seq", delimiter=',', ndmin=1, dtype=int)
    dom_vals = [ vals for vals in np.loadtxt(infile_path + ".doms", skiprows=1, delimiter=",", dtype=int) ]
    print("** Learning model")
    dcn = DCN()
    dcn.learn(train_dset, train_seqs, max_depth_prior, clt_thres_prior,
                max_cond_depth, max_depth, clt_thres, max_part_size, dom_vals)
    print("** Computing LL")
    print(dcn.avgLL(test_dset, test_seqs, evid_idx))
