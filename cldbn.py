from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree
from scipy.sparse.csgraph import depth_first_order

import numpy as np
import sys
import dbn
import hmm
import util
import os.path

usage_str = """Usage:
======
python cldbn.py <dir-path> <file-name>

Arguments:
==========
<dir-path>:\tPath to directory that contains the datasets
<file-name>:\tName of the dataset"""

# Generate scopes array for a CLT
def gen_clt_scopes(dset, continuous_flag=False, topo_order_flag=False):
  # Generate MI Matrix
  mi_matrix = util.gen_mi_matrix(dset, continuous_flag)
  if (len(dset.shape)<2 or mi_matrix.shape != (dset.shape[1],dset.shape[1])):
    return []
  # Generate CLT structure
  # X = csr_matrix(-mi_matrix)
  X = csr_matrix(-mi_matrix)
  T = minimum_spanning_tree(X)
  topo_order, parents = depth_first_order(T,0,directed=False)
  # Generate CLT scopes
  scopes = []
  V = np.arange(parents.size)
  while topo_order.size < V.size:
      v = np.setdiff1d(V, topo_order)[0]
      v_topo_order, v_parents = depth_first_order(T, v, directed=False)
      topo_order = np.append(topo_order, v_topo_order)
      parents = np.maximum(parents, v_parents)
  topo_order = topo_order if topo_order_flag else range(dset.shape[1])
  # util.remove_weak_edges(mi_matrix, topo_order, parents)
  # for i in range(dset.shape[1]):
  for i in topo_order:
    scope = np.array([i]) if parents[i]==-9999 else np.array([parents[i],i])
    scopes += [ scope ]
  return scopes

# Generate scopes array for a CLDBN
def gen_cldbn_scopes(dset, parent_flag=False, continuous_flag=False, topo_order_flag=False):
  scopes = gen_clt_scopes(dset, continuous_flag, topo_order_flag)
  if (len(scopes)==0):
    return scopes
  S = len(scopes)
  for i in range(S):
    if parent_flag:
        scopes += [ np.append(scopes[i],scopes[i]+S) ]
    else:
        scopes += [ np.append(i,scopes[i]+S) ]
  return scopes

# Learn parameters given actual domain values and scopes
def learn_cldbn(dset, seqs, dom_vals=[]):
    # Return empty model if not valid
    if (dset.ndim!=2 or dset.shape[0] != seqs.sum()):
        return np.array([]), [], []
    # Generate transition dataset
    transition_dset = hmm.create_transition_dset(dset, seqs)
    # Generate dom_vals, if not provided
    dom_vals = dom_vals if (dset.shape[1]==len(dom_vals)) else hmm.extract_all_doms(dset)
    # Flatten domains
    doms = util.flatten_dom_vals(dom_vals)
    # Generate scopes
    scopes = gen_cldbn_scopes(dset)
    # Generate CPTs
    V = len(dom_vals)
    cpts = util.learn_model_params(transition_dset, dom_vals*2, scopes)
    cpts[:V] = util.learn_model_params(dset, dom_vals, scopes[:V])
    # Return model as doms, scopes and cpts
    return doms, scopes, cpts

# Find average log-likelihood as test dataset
def cldbn_avg_LL(dset, seqs, doms, scopes, cpts):
    return dbn.dbn_avg_LL(dset, seqs, doms, scopes, cpts)

# Train and test a CLDBN from a training and test set
def cldbn_inference(dir_name, file_name, dom_vals=[]):
    # Initialize variables
    infile_path = dir_name + '/' + file_name
    # Load datasets
    train_dset = np.loadtxt(infile_path + ".train", delimiter=',', dtype=int)
    test_dset = np.loadtxt(infile_path + ".test", delimiter=',', dtype=int)
    train_seqs = np.loadtxt(infile_path + ".train.seq", delimiter=',', ndmin=1, dtype=int)
    test_seqs = np.loadtxt(infile_path + ".test.seq", delimiter=',', ndmin=1, dtype=int)
    # Train model
    dom_vals = hmm.extract_all_doms(train_dset) if (len(dom_vals)!=train_dset.shape[1]) else dom_vals
    doms, scopes, cpts = learn_cldbn(train_dset, train_seqs, dom_vals)
    # Return average log-likelihood
    # new_test_dset = util.dom_normalize_dset(test_dset, dom_vals)
    new_test_dset = test_dset
    return cldbn_avg_LL(new_test_dset, test_seqs, doms, scopes, cpts)

if __name__ == "__main__":
    # Check if arguments are present
    if(len(sys.argv)<3):
        print(usage_str)
        sys.exit(1)
    # Read in arguments
    dir_name = sys.argv[1]
    file_name = sys.argv[2]
    dom_file_name = dir_name + "/" + file_name + ".doms"
    dom_vals = []
    if os.path.exists(dom_file_name):
        dom_vals = [ vals for vals in np.loadtxt(dom_file_name, skiprows=1, delimiter=",", dtype=int) ]
    # Perform inference
    print("==========================")
    print("CLDBN Inference")
    print("==========================")
    print("Directory: " + dir_name)
    print("File Name: " + file_name)
    ll = cldbn_inference(dir_name,file_name,dom_vals)
    print("Avg LL: " + str(ll))

# python hmm.py "/Users/chiradeep/dev/cdnets/data/synth" synth.p.2.2
