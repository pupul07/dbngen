import numpy as np
import cldbn
import dbn
import hmm
import util
import ccldbn
import JTree
import cdnets
import AndDCN
import ccnet
import CNET

# np.random.seed(0)
# doms = np.array([2,2,2,2,2])
# intra_maxlinks = 3
# inter_maxlinks = 3
# scopes = dbn.gen_dbn_scopes(doms, intra_maxlinks, inter_maxlinks)
# # scopes = dbn.gen_fdbn_scopes(doms)
# max_seq_size = 10
# n = 5000
# seeds = np.arange(1000)
# # seeds = np.array([202])
# train_seqs = np.full(n/max_seq_size,max_seq_size)
# train_seqs = train_seqs if train_seqs.sum()==n else np.append(train_seqs,n%max_seq_size)
# test_seqs = np.full(n/(10*max_seq_size),max_seq_size)
# test_seqs = test_seqs if test_seqs.sum()==(n/10) else np.append(test_seqs,n%(10*max_seq_size))
#
# max_diff = -9999999.0
# max_train_dset = np.array([])
# max_test_dset = np.array([])
# max_cpts = []
#
# for s in range(seeds.size):
#   print("Processing model "+str(s+1))
#   # Set seed
#   np.random.seed(seeds[s])
#   # Train DBN
#   cpts = dbn.gen_dbn_cpts(doms, scopes)
#   # train_seqs = util.gen_seqs(n, max_seq_size)
#   # test_seqs = util.gen_seqs(n/10, max_seq_size)
#   train_dset = dbn.gen_dbn_samples(n, train_seqs, doms, scopes, cpts)
#   test_dset = dbn.gen_dbn_samples(n/10, test_seqs, doms, scopes, cpts)
#   dbn_ll = dbn.dbn_avg_LL(test_dset, test_seqs, doms, scopes, cpts)
#   print(dbn_ll)
#   # Train HMM
#   # hmm_doms = hmm.extract_all_doms(train_dset)
#   hmm_doms = [ np.arange(d) for d in doms ]
#   hmm_scopes = hmm.create_prior_probs(train_dset)
#   hmm_cpts = hmm.create_transition_cpts(train_dset, train_seqs, hmm_doms)
#   hmm_ll = hmm.avg_LL(test_dset, test_seqs, hmm_doms, hmm_scopes, hmm_cpts)
#   print(hmm_ll)
#   # Choose most disparate model
#   ll_diff = dbn_ll-hmm_ll
#   print(ll_diff)
#   if ll_diff > max_diff:
#     max_diff = ll_diff
#     max_train_dset = train_dset
#     max_train_seqs = train_seqs
#     max_test_dset = test_dset
#     max_test_seqs = test_seqs
#     max_cpts = cpts
#
# print(max_diff)
# path = "/Users/chiradeep/dev/cdnets/data/synth"
# model_name = "model1.p.3.3"
# util.save_dbn_model(path, model_name, doms, scopes, max_cpts)
# np.savetxt(path+"/"+model_name+".train", max_train_dset, fmt="%i", delimiter=",")
# np.savetxt(path+"/"+model_name+".train.seq", train_seqs.reshape(1,train_seqs.size), fmt="%i", delimiter=", ")
# np.savetxt(path+"/"+model_name+".test", max_test_dset, fmt="%i", delimiter=",")
# np.savetxt(path+"/"+model_name+".test.seq", test_seqs.reshape(1,test_seqs.size), fmt="%i", delimiter=", ")


# # LL Calculation Test
# doms = np.array([3,2])
# scopes = dbn.gen_fdbn_scopes(doms)
# cpts = [ np.array([0.1,0.7,0.2]), np.array([0.7,0.3,0.2,0.8,0.1,0.9]),
#          np.array([0.3,0.2,0.5,0.4,0.4,0.2,0.7,0.1,0.2,0.4,0.3,0.3,0.5,0.4,0.1,0.2,0.2,0.6]),
#          np.array([0.2,0.8,0.9,0.1,0.4,0.6,0.1,0.9,0.6,0.4,0.7,0.3,\
#                    0.5,0.5,0.8,0.2,0.4,0.6,0.7,0.3,0.1,0.9,0.5,0.5,\
#                    0.7,0.3,0.9,0.1,0.5,0.5,0.3,0.7,0.4,0.6,0.8,0.2]) ]
#
# dset = np.array([[2,0],[1,1],[2,1],[1,1]])
# seqs = np.array([4])
# ll = dbn.dbn_avg_LL(dset, seqs, doms, scopes, cpts)
# print(abs(-2.7871-ll) < 0.0001)


# # Initialize variables
# dir_name = "/Users/chiradeep/dev/cdnets/data/pendigits"
# file_name = "pendigits.2.bin"
# infile_path = dir_name + '/' + file_name
# train_dset = np.loadtxt(infile_path + ".train", delimiter=',', dtype=int)
# train_seqs = np.loadtxt(infile_path + ".train.seq", delimiter=',', ndmin=1, dtype=int)
# dom_vals = [ vals for vals in np.loadtxt(infile_path + ".doms", skiprows=1, delimiter=",", dtype=int) ]
#
# dcn = DCN.DCN()
# dcn.learn(train_dset, train_seqs, 0, 1, 1, 0, 1, 1, dom_vals)
# evid_idx = np.array([0,4])
# evid_vals = np.array([0,1])
# msg = dcn.transition_model.multiplyAndMarginalize(dcn.prior_model)
#
# doms = util.flatten_dom_vals(dom_vals)
# curr_idx = np.arange(msg.size)
# curr_assign = np.zeros(msg.size, dtype=int)
# joint_dist = np.zeros(doms.prod())
#
# for i in range(doms.prod()):
#     joint_dist[i] = msg.probEvid(curr_idx, curr_assign)
#     util.go_to_next_assign(curr_assign, doms)
# print(joint_dist)
# print(joint_dist.sum())

# Initialize variables
dir_name = "/Users/chiradeep/dev/cdnets/data/diabetes"
file_name = "diabetes.16.bin"
evid_idx = np.array([12,13,14,15,16,17,18,19,20,21,22,23])
# evid_idx = np.array([])
dom_vals = [ np.arange(2) ] * 25
# Test model
print("HMM: " + str(hmm.hmm_inference(dir_name, file_name, evid_idx, dom_vals)))
# print("CLDBN: " + str(cldbn.cldbn_inference(dir_name, file_name, dom_vals)))
# print("DBN: "+str(dbn.dbn_inference(dir_name, file_name, dom_vals)))
# print("CCLDBN: " + str(ccldbn.ccldbn_inference(dir_name, file_name, dom_vals)))

# dir_name = '/Users/chiradeep/dev/cdnets/data/japanvowels'
# file_name = 'japanvowels.8.bin'
# infile_path = dir_name + '/' + file_name
#
# train_dset = np.loadtxt(infile_path + ".train", delimiter=',', dtype=int)
# test_dset = np.loadtxt(infile_path + ".test", delimiter=',', dtype=int)
# train_seqs = np.loadtxt(infile_path + ".train.seq", delimiter=',', ndmin=1, dtype=int)
# test_seqs = np.loadtxt(infile_path + ".test.seq", delimiter=',', ndmin=1, dtype=int)
# transition_dset = hmm.create_transition_dset(train_dset, train_seqs)
#
# real_evid_idx = np.arange(test_dset.shape[1]/2,test_dset.shape[1])
# evid_var = np.arange(train_dset.shape[1])
# query_var =  np.arange(train_dset.shape[1], 2*train_dset.shape[1])
# evid_arr = transition_dset[:,evid_var]
# query_arr = transition_dset[:, query_var]
# function_type = "LR"
# alpha = 0.01
# print('Evidence indices: '+str(real_evid_idx))
#
# print('Training prior model')
# prior_model = CNET.CNET(depth=0)
# prior_model.learnStructure(train_dset)
# # prior_model = cdnets.CNet()
# # prior_model.learn(train_dset, 1, 1)
#
# print('Training transition model')
# transition_model = ccnet.CNET_CLT([], 2)
# transition_model.learnStructure(evid_arr, query_arr, function_type, alpha)
#
# print('Testing model')
# # total_ll = 0.0
# # cum_test_seqs = np.append(0,test_seqs[:-1]).cumsum()
# # for s in range(test_seqs.shape[0]):
# #     total_ll += prior_model.computeLL(test_dset[cum_test_seqs[s]:cum_test_seqs[s]+1])
# #     # total_ll += prior_model.LL(test_dset[cum_test_seqs[s]])
# #     for i in range(cum_test_seqs[s]+1, cum_test_seqs[s]+test_seqs[s]):
# #         total_ll += transition_model.computeLL(test_dset[i-1:i],test_dset[i:i+1])
#
# total_ll = 0.0
# total_ell = 0.0
# K = 10
# cum_test_seqs = np.append(0,test_seqs[:-1]).cumsum()
# evid_idx = np.in1d(np.arange(test_dset.shape[1]),real_evid_idx)
# for s in range(test_seqs.shape[0]):
#     total_ll += prior_model.computeLL(test_dset[cum_test_seqs[s]:cum_test_seqs[s]+1])
#     prev_particles = prior_model.weighted_samples(K ,evid_idx, test_dset[cum_test_seqs[s]][evid_idx])
#     total_ll += prior_model.computeLL(test_dset[cum_test_seqs[s]:cum_test_seqs[s]+1])
#     total_ell += np.log(prior_model.prob_evid(prior_model.tree, evid_idx, test_dset[cum_test_seqs[s]][evid_idx]))
#     for i in range(cum_test_seqs[s]+1, cum_test_seqs[s]+test_seqs[s]):
#         new_particles = []
#         i_prob_evid = 0.0
#         for particle in prev_particles:
#             weight, vals = particle
#             evid = test_dset[i][evid_idx]
#             curr_model = transition_model.convert_to_cnet(vals.reshape(1,vals.size))
#             i_prob_evid += np.exp(-weight) * curr_model.prob_evid(curr_model.tree, evid_idx, evid)
#             new_particles += curr_model.weighted_samples(K, evid_idx, test_dset[i][evid_idx])
#         prev_particles = sorted(new_particles, key=lambda x: x[0])[:K]
#         total_ll += transition_model.computeLL(test_dset[i-1:i],test_dset[i:i+1])
#         total_ell += np.log(i_prob_evid)
#
# print((total_ll-total_ell)/ test_dset.shape[0])