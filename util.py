from sklearn.metrics import mutual_info_score
from sklearn.feature_selection import mutual_info_regression

import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

# Remove weak edges from a CLT
def remove_weak_edges(mi_matrix, topo_order, parents, thres=0.05):
  for t in topo_order:
    if parents[t]<0:
      continue
    parents[t] = parents[t] if mi_matrix[t,parents[t]]>=thres else -9999

# Generate mutual information matrix
def gen_mi_matrix(dset, continuous_flag=False):
  if(len(dset.shape)<2):
    return np.array([[]])
  mi_matrix = np.zeros((dset.shape[1],dset.shape[1]))
  if (dset.shape[0]==0):
      return mi_matrix
  for i in range(mi_matrix.shape[0]):
    for j in range(i+1,mi_matrix.shape[1]):
      if(continuous_flag):
        mi_matrix[i,j] = mutual_info_regression(dset[:,i:i+1],dset[:,j], n_neighbors=5, random_state=1991)
      else:
        mi_matrix[i,j] = mutual_info_score(dset[:,i],dset[:,j])
      mi_matrix[j,i] = mi_matrix[i,j]
  return mi_matrix

# Generate mutual information matrix
def gen_mi_matrix_select(dset, select_idx=np.array([]), continuous_flag=False):
  if(len(dset.shape)<2 or select_idx.ndim!=1):
    return np.array([[]])
  if(select_idx.size==0):
    return gen_mi_matrix(dset, continuous_flag)
  mi_matrix = np.zeros((dset.shape[1],select_idx.size))
  if (dset.shape[0]==0):
      return mi_matrix
  for i in np.arange(dset.shape[1]):
    for j in np.arange(select_idx.size):
      s = select_idx[j]
      if(continuous_flag):
        mi_matrix[i,j] = mutual_info_regression(dset[:,i:i+1],dset[:,s], n_neighbors=5, random_state=1991)
      else:
        mi_matrix[i,j] = mutual_info_score(dset[:,i],dset[:,s])
  return mi_matrix

# Given a set of values and the domains, generate address
def calc_address(vals, doms=np.array([])):
  if not doms.size:
    doms = np.full(vals.shape[0], 2, dtype=int)
  if (len(vals) != len(doms)):
    return -1
  addr = 0
  cum_doms = np.append(np.cumprod(doms[::-1])[::-1][1:],1)
  for v in reversed(range(len(vals))):
    addr += cum_doms[v] * vals[v]
  return addr

# Given an address and the domains, calculate the value assignments
def calc_vals_from_address(addr, doms):
  # Validation
  if doms.ndim != 1 or addr<0 or addr>=doms.prod():
    return np.array([], dtype=int)
  # Initialization
  vals = np.zeros(doms.size, dtype=int)
  block_sizes = np.append(doms[::-1].cumprod()[::-1][1:],1)
  # Calculate vals
  for v in range(vals.size):
    vals[v] = int(addr/block_sizes[v])
    addr = addr%block_sizes[v]
    if (addr==0):
      break
  # Return vals
  return vals

# Given an assignment, goes to next assignment
def go_to_next_assign(curr_assign, doms):
  # If invalid, do nothing
  if(doms.shape[0] != curr_assign.shape[0] or curr_assign.shape[0]==0):
    return
  # Find index to fill from
  start = np.append(-1,np.argwhere(1-(curr_assign+1==doms)))[-1]
  # Fill everything after start with 0
  curr_assign[start+1:].fill(0)
  # Increment element at start index by 1 as long as it's not -1
  curr_assign[start] += 1 if(start>=0) else 0

# Same as go_to_next_assign but returns a new ndarray
def calc_next_assign(curr_assign, doms):
    new_assign = np.array(curr_assign)
    go_to_next_assign(new_assign, doms)
    return new_assign

# Function to draw a DBN model and save it as a .png file
def draw_dbn_model(dir_path, file_name, doms, scopes):
    # Initialize graph
    G = nx.DiGraph()
    # Add all nodes
    V = doms.shape[0]
    node_labels = {}
    for v in range(2*V):
        suffix = "_t-1" if v<V else "_t"
        node_labels[v] = "x" + str(v%V) + suffix
        G.add_node(v)
    # Add edges from scope
    for scope in scopes:
        i = scope[-1]
        for j in scope[:-1]:
            G.add_edge(j,i)
    # Save model
    outfile_path = dir_path + "/" + file_name + ".png"
    nx.draw(G, with_labels=True, labels=node_labels, font_size=8,
            node_size=1000, node_color=['b']*V+['r']*V)
    plt.savefig(outfile_path)
    plt.clf()

# Function to load DBN model from file
def load_dbn_model(dir_path, file_name):
    # Initialize variables
    infile_path = dir_path + "/" + file_name + ".dbn"
    doms = np.array([], dtype=int)
    scopes = []
    cpts = []
    with open(infile_path, "r") as infile:
        lines = infile.readlines()
        V = int(lines[0]) if len(lines) else 1
        if (len(lines) >= (4*V)+2):
            # Read in doms
            doms = np.fromstring(lines[1].strip(), dtype=int, sep=",")
            # Read in scopes
            for line in lines[2:(2*V)+2]:
                scopes += [ np.fromstring(line.strip(), dtype=int, sep=",") ]
            # Read in CPTs
            for line in lines[(2*V)+2:(4*V)+2]:
                cpts += [ np.fromstring(line.strip(), dtype=float, sep=",") ]
    return (doms, scopes, cpts)

# Function to save DBN model to file
def save_dbn_model(dir_path, file_name, doms, scopes, cpts):
    # Initialize variables
    outfile_path = dir_path + "/" + file_name + ".dbn"
    with open(outfile_path, "w") as outfile:
        # Write number of variables
        outfile.write(str(doms.size)+"\n")
        # Write domains
        outfile.write(', '.join(map(str,doms))+"\n")
        # Write scopes
        for scope in scopes:
            outfile.write(', '.join(map(str,scope))+"\n")
        # Write CPTs
        for cpt in cpts:
            outfile.write(', '.join(map(str,cpt))+"\n")

# Function to randomly assign sequence length between [2,max_seq_length] given number of samples
def gen_seqs(n, max_seq_size):
  # Check for illegal values
  if (n<1 or max_seq_size<=1):
    return np.array([], dtype=int)
  # Initialize variables
  count = 0
  seqs = []
  # Generate sequence-by-sequence
  while n-count>max_seq_size:
    seq = np.random.randint(2,max_seq_size+1)
    seqs += [ seq ]
    count += seq
  seqs += [ n-count ] if (n-count>0) else []
  # Return seqs
  return np.array(seqs, dtype=int)

# Flatten domain values to extract domain sizes
def flatten_dom_vals(dom_vals):
    doms = np.zeros(len(dom_vals), dtype=int)
    for d in range(doms.shape[0]):
        doms[d] = dom_vals[d].size
    return doms

# Learn parameters given actual domain values and scopes
def learn_model_params(dset, dom_vals, scopes):
    cpts = []
    # Verify if parameters are valid
    if (dset.shape[1] != len(dom_vals) or len(dom_vals) != len(scopes)):
        return cpts
    # Initialize variables
    V = len(scopes)
    doms = flatten_dom_vals(dom_vals)
    max_dom_size = 0
    for c in range(dset.shape[1]):
        max_dom_size = max(max_dom_size, np.unique(dset[:, c]).size)
    # smooth_factor = (dset.shape[0]*0.001) / max_dom_size
    smooth_factor = (max(1,dset.shape[0]) * 0.001) / doms.size
    # Learn CPT for each variable
    for v in range(V):
        # First, extract relevant information
        scope = scopes[v]
        curr_dom_vals = np.array(dom_vals)[scope]
        curr_doms = doms[scope]
        curr_assign = np.zeros(scope.shape[0], dtype=int)
        cpt = np.zeros((curr_doms[-1],curr_doms[:-1].prod()))
        # Next, calculate the value of each cpt row
        for j in range(curr_doms[:-1].prod()):
            for i in range(curr_doms[-1]):
                matches = True
                for k in range(curr_assign.shape[0]):
                    matches *= (dset[:,scope[k]] == curr_dom_vals[k][curr_assign[k]])
                cpt[i,j] = matches.sum() + smooth_factor
                go_to_next_assign(curr_assign, curr_doms)
        # Add CPT to CPTs
        z = cpt.sum(axis=0).reshape(1,curr_doms[:-1].prod())
        cpts += [ (cpt / z).T.flatten() ]
    # Return all CPTs
    return cpts

# Domain normalize a dataset
def dom_normalize_dset(dset, dom_vals):
  new_dset = np.array(dset)
  for c in range(dset.shape[1]):
    if (np.unique(dset[:,c]).size != dom_vals[c].size):
      return np.array([])
    dict = zip(dom_vals[c],np.arange(dom_vals[c].shape[0]))
    for k, v in dict: new_dset[dset[:,c]==k,c] = v
  return new_dset

# Remove evidence from single factor
def remove_evid(scope, doms, cpt, evid_idx, evid_vals):
    # Validation
    if(scope.ndim!=1 or cpt.ndim!=1 or doms.ndim!=1 or scope.size != doms.size or cpt.size!=doms.prod() or \
        evid_idx.ndim!=1 or evid_vals.ndim!=1 or evid_idx.size!=evid_vals.size or (evid_idx<0).sum()):
        return cpt, doms
    # Compute scope indices w.r.t. evidence
    scope_evid_idx = np.nonzero(np.in1d(scope, evid_idx))[0]
    scope_non_evid_idx = np.setdiff1d(np.arange(scope.size),scope_evid_idx)
    scope_evid_vals = evid_vals[np.hstack((np.nonzero(np.in1d(evid_idx, scope[:-1]))[0],
                                           np.nonzero(np.in1d(evid_idx, scope[-1:]))[0]))]
    # Validate if evid_vals has anything outside of the domains
    if (doms[scope_evid_idx]-scope_evid_vals<=0).sum():
        return cpt, doms
    # Initialize new_doms and new_cpt
    new_doms = np.array(doms)
    new_doms[scope_evid_idx] = 1
    new_cpt = np.zeros(new_doms.prod(), dtype=float)
    # Compute val/assignment vectors
    new_vals = np.zeros(scope_non_evid_idx.size, dtype=int)
    all_vals = np.zeros(scope.size, dtype=int)
    all_vals[scope_evid_idx] = scope_evid_vals
    # Fill in CPT
    for i in range(new_doms.prod()):
        all_vals[scope_non_evid_idx] = new_vals
        addr_all = calc_address(all_vals, doms)
        addr_new = calc_address(new_vals, new_doms[scope_non_evid_idx])
        new_cpt[addr_new] += cpt[addr_all]
        go_to_next_assign(new_vals, new_doms[scope_non_evid_idx])
    # Return new CPT and new doms
    return new_cpt, new_doms

# Multiply and marginalize two factors
def multiply_and_marginalize(scope1, doms1, factor1, scope2, doms2, factor2, marg_scope=np.array([]), to_normalize=False):
  # Validation
  if (scope1.ndim!=1 or doms1.ndim!=1 or factor1.ndim!=1 or scope1.size != doms1.size or \
      doms1.prod() != factor1.size or scope2.ndim!=1 or doms2.ndim!=1 or factor2.ndim!=1 or \
      scope2.size != doms2.size or doms2.prod() != factor2.size or marg_scope.ndim!=1 or \
      np.setdiff1d(marg_scope,np.union1d(scope1,scope2)).size>0):
      return np.array([]), np.array([]), np.array([])
  # Generate dictionaries
  scope1_dict = { key: val for key, val in zip(scope1,np.arange(len(scope1))) }
  scope2_dict = { key: val for key, val in zip(scope2,np.arange(len(scope2))) }
  # Generate joint scope
  joint_scope = np.union1d(scope1,scope2)
  joint_doms = np.array([ doms1[scope1_dict[var]] if var in scope1_dict else doms2[scope2_dict[var]] for var in joint_scope ])
  # Generate marginal scope
  marg_scope = marg_scope if marg_scope.size else joint_scope
  marg_doms = np.array([ doms1[scope1_dict[var]] if var in scope1_dict else doms2[scope2_dict[var]] for var in marg_scope ])
  marg_dict = { key: val for key, val in zip(marg_scope,np.arange(len(marg_scope))) }
  # Generate selects
  scope1_select = np.empty(scope1.shape, dtype=int)
  scope2_select = np.empty(scope2.shape, dtype=int)
  marg_select = np.empty(marg_scope.shape, dtype=int)
  for v in range(len(joint_scope)):
    var = joint_scope[v]
    if var in scope1_dict:
      scope1_select[scope1_dict[var]] = v
    if var in scope2_dict:
      scope2_select[scope2_dict[var]] = v
    if var in marg_dict:
      marg_select[marg_dict[var]] = v
  # Multiply and marginalize
  joint_assign = np.zeros(joint_scope.shape, dtype=int)
  marg_factor = np.zeros(marg_doms.prod())
  for i in range(joint_doms.prod()):
    addr1 = calc_address(joint_assign[scope1_select], doms1)
    addr2 = calc_address(joint_assign[scope2_select], doms2)
    marg_addr = calc_address(joint_assign[marg_select], marg_doms)
    marg_factor[marg_addr] += factor1[addr1] * factor2[addr2]
    go_to_next_assign(joint_assign, joint_doms)
  # Return new scope, doms and factor
  marg_factor = marg_factor / marg_factor.sum() if to_normalize and marg_factor.sum()>0.0 else marg_factor
  return marg_scope, marg_doms, marg_factor

# Marginalize a single factor over a given scope
def marginalize(scope, doms, factor, marg_scope, to_normalize=False):
  # Validation
  if (scope.ndim != 1 or doms.ndim != 1 or factor.ndim != 1 or scope.size != doms.size or \
      doms.prod() != factor.size or marg_scope.size == 0 or np.setdiff1d(marg_scope,scope).size>0):
      return scope, doms, factor
  # Create dictionary and select
  scope_dict = { key: val for key, val in zip(scope,np.arange(len(scope))) }
  marg_dict = { key: val for key, val in zip(marg_scope,np.arange(len(marg_scope))) }
  marg_doms = np.array([ doms[scope_dict[var]] for var in marg_scope ])
  marg_select = np.empty(marg_scope.shape, dtype=int)
  for v in range(len(scope)):
    var = scope[v]
    if var in marg_dict:
      marg_select[marg_dict[var]] = v
  # Marginalize
  full_assign = np.zeros(scope.shape, dtype=int)
  marg_factor = np.zeros(marg_doms.prod())
  for i in range(doms.prod()):
    addr = calc_address(full_assign, doms)
    marg_addr = calc_address(full_assign[marg_select], marg_doms)
    marg_factor[marg_addr] += factor[addr]
    go_to_next_assign(full_assign, doms)
  # Return new scope, doms and factor
  marg_factor = marg_factor / marg_factor.sum() if to_normalize and marg_factor.sum()>0.0 else marg_factor
  return marg_scope, marg_doms, marg_factor

# Returns the address of the entry of a given factor table that has the highest value
def factor_argmax(factor, doms):
  if doms.ndim != 1 or factor.ndim != 1 or factor.size != doms.prod():
    return -1
  addr = np.argmax(factor)
  return calc_vals_from_address(addr, doms)
