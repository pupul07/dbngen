import numpy as np
import sys

from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree
from scipy.sparse.csgraph import depth_first_order

class JTree:
    def __init__(self):
        self.topo_order = np.array([], dtype=int)
        self.parents = np.array([], dtype=int)
        self.cluster_scopes = np.array([])
        self.cluster_assigns = np.array([])
        self.bn_scopes = []
        self.bn_cpts = []
    # Learns JTree structure along topological ordering
    def learnStructure(self, bn_scopes, bn_cpts):
        # Store scopes and cpts
        self.bn_scopes = bn_scopes
        self.bn_cpts = bn_cpts
        # Create adjacency list with moralization
        adj_list = [ np.array([],dtype=int) ] * len(bn_scopes)
        assigns = [ np.array([],dtype=int) ] * len(bn_scopes)
        for bn_scope in bn_scopes:
            node = bn_scope[-1]
            adj_list[node] = np.union1d(adj_list[node],node)
            assigns[bn_scope[-1]] = np.union1d(assigns[node],node)
            if len(bn_scope)>1:
                for parent in bn_scope[:-1]:
                    adj_list[parent] = np.union1d(adj_list[parent],bn_scope)
                    if len(assigns[node])>0:
                        assigns[parent] = np.union1d(assigns[parent], node)
                        assigns[node] = np.setdiff1d(assigns[node], node)
        # Perform schematic variable elimination
        cluster_scopes = []
        cluster_assigns = []
        # for node in range(len(adj_list)):
        nodes = [ bn_scope[-1] for bn_scope in bn_scopes ]
        for node in nodes:
            cluster = adj_list[node]
            assign = assigns[node]
            if len(cluster_scopes)==0 or len(np.setdiff1d(cluster,cluster_scopes[-1]))>0:
                cluster_scopes += [ cluster ]
                cluster_assigns += [ assign ]
            else:
                cluster_assigns[-1] = np.union1d(cluster_assigns[-1],assign)
            for neighbor_node in np.setdiff1d(cluster,np.array([node])):
                adj_list[neighbor_node] = np.setdiff1d(np.union1d(adj_list[neighbor_node], cluster), np.array([node]))
        # Create weighted adjacency matrix to compute cluster tree
        cluster_adj_mat = np.zeros((len(cluster_scopes), len(cluster_scopes)), dtype=int)
        for i in range(len(cluster_scopes)):
            for j in range(len(cluster_scopes)):
                if i==j:
                    continue
                cluster_adj_mat[i][j] = len(np.intersect1d(cluster_scopes[i], cluster_scopes[j]))
                cluster_adj_mat[j][i] = cluster_adj_mat[i][j]
        # Find max spanning tree and return junction tree
        jtree = minimum_spanning_tree(csr_matrix(-cluster_adj_mat))
        self.topo_order, self.parents = depth_first_order(jtree, 0, directed=False)
        self.cluster_scopes = np.array(cluster_scopes)
        self.cluster_assigns = np.array(cluster_assigns)
    def __str__(self, verbose=False):
        docstr = "Topo Order: " + str(self.topo_order) + "\n" + \
                    "Parents: " + str(self.parents) + "\n" + \
                    "Cluster Scopes: " + str(self.cluster_scopes)
        if not verbose:
            return docstr
        return docstr + "\nBN Scopes: " + str(self.bn_scopes) + "\n" + \
                            "Scope Assignments: " + str(self.cluster_assigns)

def first_index(arr):
    return arr[0]

def gen_bk_scopes(jtree, max_cluster_size=None):
    added_nodes = np.array([], dtype=int)
    # cluster_scopes = sorted(jtree.cluster_scopes, key=len)
    cluster_scopes = jtree.cluster_scopes
    bk_scopes = []
    full_scope = np.array([], dtype=int)
    for cluster_scope in cluster_scopes:
        bk_scopes += [ np.setdiff1d(cluster_scope, added_nodes) ]
        full_scope = np.append(full_scope, bk_scopes[-1])
        added_nodes = np.union1d(added_nodes, cluster_scope)
    # return sorted([ bk_scope for bk_scope in bk_scopes if len(bk_scope)>0 ], key=first_index)
    if not str(max_cluster_size).isdigit():
        return bk_scopes
    num_chunks = np.ceil((len(full_scope)*1.0)/max_cluster_size).astype(int)
    return np.array_split(full_scope, num_chunks)


if __name__ == "__main__":
    # bn_scopes = [ np.array([0]), np.array([1]), np.array([0,1,2]), np.array([2,3]), np.array([2,4]) ]
    bn_scopes = [ np.array([0]), np.array([1]), np.array([2]), np.array([3]), np.array([4]),
                    np.array([0,1,5]), np.array([2,6]), np.array([3,4,7]), np.array([5,6,7,8]),
                    np.array([0,1,9]), np.array([2,3,4,10]), np.array([9,11]), np.array([10,12]), np.array([10,13]) ]
    bn_cpts = []
    jtree = JTree()
    jtree.learnStructure(bn_scopes, bn_cpts)
    bk_scopes = gen_bk_scopes(jtree)
    print(bk_scopes)
